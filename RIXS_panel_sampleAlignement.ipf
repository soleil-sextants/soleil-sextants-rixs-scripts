﻿#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.


Sample alignement Panel pour les listes : Sample_tx / Sample_tz





function RIXS_sampleAlignement_DLG()

// There or not
	doWindow /F RIXSsampleAlignementPanel
	if(V_flag == 1)
	
		return 0
	endif
	
// DLG frame
	PauseUpdate; Silent 1		// building window...
	NewPanel /N=RIXSsampleAlignementPanel /W=(353,45,1432,763) as "RIXS Spectra : Sample alignement"
	SetDrawLayer UserBack
	DrawText 256,33,"Sample tx"
	DrawText 256,392,"Sample tz"
	
	
// Sample-tx
	display /N=visu_tx /HOST=RIXSsampleAlignementPanel /W=(21,38,581,354)
	
	CheckBox checkCursorsSampleAlignement_tx,pos={599.00,36.00},size={86.00,16.00},proc=CheckProc_cursors_sampleAlignement,title="Cursors A & B"
	CheckBox checkCursorsSampleAlignement_tx,value= 0
	
	CheckBox checkTEYsampleAlignement_tx,pos={790.00,37.00},size={31.00,16.00},proc=CheckProc_TEY_TFY_sampleAlignement,title="TEY"
	CheckBox checkTEYsampleAlignement_tx,value= 1
	
	CheckBox checkTFYsampleAlignement_tx,pos={847.00,37.00},size={31.00,16.00},proc=CheckProc_TEY_TFY_sampleAlignement,title="TFY"
	CheckBox checkTFYsampleAlignement_tx,value= 1
	
	ListBox listRIXSsampleAlignement_tx,pos={598.00,63.00},size={282.00,293.00},proc=ListBoxProc_sampleAlignement_tx
	ListBox listRIXSsampleAlignement_tx,mode= 4
	ListBox listRIXSsampleAlignement_tx, listWave = root:RIXS:Variables:sampleAlignement_tx
	ListBox listRIXSsampleAlignement_tx, selWave = root:RIXS:Variables:selSampleAlignement_tx
	


// Sample-tz
	display /N=visu_tz /HOST=RIXSsampleAlignementPanel /W=(22,397,581,710)
	
	CheckBox checkCursorsSampleAlignement_tz,pos={601.00,395.00},size={86.00,16.00},proc=CheckProc_cursors_sampleAlignement,title="Cursors A & B"
	CheckBox checkCursorsSampleAlignement_tz,value= 0
	
	CheckBox checkTEYsampleAlignement_tz,pos={774.00,394.00},size={31.00,16.00},proc=CheckProc_TEY_TFY_sampleAlignement,title="TEY"
	CheckBox checkTEYsampleAlignement_tz,value= 1
	
	CheckBox checkTFYsampleAlignement_tz,pos={847.00,395.00},size={31.00,16.00},proc=CheckProc_TEY_TFY_sampleAlignement,title="TFY"
	CheckBox checkTFYsampleAlignement_tz,value= 1

	ListBox listRIXSsampleAlignement_tz,pos={599.00,418.00},size={282.00,293.00},proc=ListBoxProc_sampleAlignement_tz
	ListBox listRIXSsampleAlignement_tz,mode= 4
	ListBox listRIXSsampleAlignement_tz, listWave = root:RIXS:Variables:sampleAlignement_tz
	ListBox listRIXSsampleAlignement_tz, selWave = root:RIXS:Variables:selSampleAlignement_tz
	
	
// Contextual data	
	SetVariable setvarSampleAlignementPolarization,pos={906.00,567.00},size={166.00,19.00},bodyWidth=100,disable=2,title="Polarization"
	SetVariable setvarSampleAlignementPolarization,limits={-inf,inf,0},value= root:RIXS:Variables:polarization
	
	SetVariable setvarSampleAlignementEnergy,pos={932.00,591.00},size={140.00,19.00},bodyWidth=100,disable=2,title="Energy"
	SetVariable setvarSampleAlignementEnergy,limits={-inf,inf,0},value= root:RIXS:Variables:sample_Rz
	
	SetVariable setvarSampleAlignementRz,pos={913.00,615.00},size={159.00,19.00},bodyWidth=100,disable=2,title="Sample Rz"
	SetVariable setvarSampleAlignementRz,limits={-inf,inf,0},value= root:RIXS:Variables:sample_Tx
	
	SetVariable setvarSampleAlignementTs,pos={915.00,640.00},size={157.00,19.00},bodyWidth=100,disable=2,title="Sample Ts"
	SetVariable setvarSampleAlignementTs,limits={-inf,inf,0},value= root:RIXS:Variables:sample_Ts

	
// Ciao	
	Button buttonCloseRIXSsampleAlignementPanel,pos={944.00,689.00},size={129.00,23.00},proc=btnProc_closeRIXSsampleAlignementPanel,title="Close"

	return 0
end



Function CheckProc_cursors_sampleAlignement(ctrlName,checked) : CheckBoxControl
	String ctrlName
	Variable checked
	
	controlInfo /W=RIXSsampleAlignementPanel checkCursors
	
	if (V_Value == 0)
	
		HideInfo /W=RIXSsampleAlignementPanel
	else
	
		ShowInfo /CP=0 /W=RIXSsampleAlignementPanel		// /CP=0 ==> cursours A & B
	endif
End




//checkTEYsampleAlignement_tx
//checkTFYsampleAlignement_tx
//
//checkTEYsampleAlignement_tz
//checkTFYsampleAlignement_tz

Function CheckProc_TEY_TFY_sampleAlignement(ctrlName,checked) : CheckBoxControl
	String ctrlName
	Variable checked
	
	
	WAVE 	selSampleAlignement_tx	= root:RIXS:Variables:selSampleAlignement_tx
	WAVE 	selSampleAlignement_tz	= root:RIXS:Variables:selSampleAlignement_tz
	
	variable checkTEYvalue_tx, checkTFYvalue_tx
	variable checkTEYvalue_tz, checkTFYvalue_tz
	
	string sample
	
	strSwitch(ctrlName)
	
		case "checkTEYsampleAlignement_tx" :
		
			Make /O/N=(numpnts(selSampleAlignement_tx)) root:RIXS:Variables:selBufferWave = selSampleAlignement_tx
			sample = "sample_tx"
			break
			
		case "checkTFYsampleAlignement_tx" :
		
			Make /O/N=(numpnts(selSampleAlignement_tx)) root:RIXS:Variables:selBufferWave = selSampleAlignement_tx
			sample = "sample_tx"
			break
			
		case "checkTEYsampleAlignement_tz" :
		
			Make /O/N=(numpnts(selSampleAlignement_tz)) root:RIXS:Variables:selBufferWave = selSampleAlignement_tz
			sample = "sample_tz"
			break
			
		case "checkTFYsampleAlignement_tz" :
		
			Make /O/N=(numpnts(selSampleAlignement_tz)) root:RIXS:Variables:selBufferWave = selSampleAlignement_tz
			sample = "sample_tz"
			break
	endSwitch
	

	controlInfo /W=RIXSsampleAlignementPanel checkTEYsampleAlignement_tx ; checkTEYvalue_tx = V_Value
	controlInfo /W=RIXSsampleAlignementPanel checkTFYsampleAlignement_tx ; checkTFYvalue_tx = V_Value
	if (checkTEYvalue_tx == 0 && checkTFYvalue_tx == 0)
	
		DoAlert /T="!!! Checkbox !!!" 0, "Checkbox TEY and TFY unchecked. Check at least one of them"
		CheckBox $ctrlName win = RIXSsampleAlignementPanel, value= 1
		return 0
	endif
	
	
	controlInfo /W=RIXSsampleAlignementPanel checkTEYsampleAlignement_tz ; checkTEYvalue_tz = V_Value
	controlInfo /W=RIXSsampleAlignementPanel checkTFYsampleAlignement_tz ; checkTFYvalue_tz = V_Value
	if (checkTEYvalue_tz == 0 && checkTFYvalue_tz == 0)
	
		DoAlert /T="!!! Checkbox !!!" 0, "Checkbox TEY and TFY unchecked. Check at least one of them"
		CheckBox $ctrlName win = RIXSsampleAlignementPanel, value= 1
		return 0
	endif
	
	WAVE selBufferWave = root:RIXS:Variables:selBufferWave
	waveStats /Q selBufferWave
	if (V_sum != 0)
	
		// Envoi pour nouveau remplissage graphe avec la la sélection en vigueur
		RIXS_sampleAlignement_selectOnClic(sample)
	endif
		
	return 0
End 






Function ListBoxProc_sampleAlignement_tx(lba) : ListBoxControl
	STRUCT WMListboxAction &lba

	Variable row = lba.row
	Variable col = lba.col
	WAVE/T/Z listWave = lba.listWave
	WAVE/Z selWave = lba.selWave
	
	switch( lba.eventCode )
		
		case 5: // cell selection plus shift key
			string sample_tx = "sample_tx"
			RIXS_sampleAlignement_selectOnClic(sample_tx)
			break
	endswitch

	return 0  
End




Function ListBoxProc_sampleAlignement_tz(lba) : ListBoxControl
	STRUCT WMListboxAction &lba

	Variable row = lba.row
	Variable col = lba.col
	WAVE/T/Z listWave = lba.listWave
	WAVE/Z selWave = lba.selWave
	
	switch( lba.eventCode )
		
		case 5: // cell selection plus shift key
			string sample_tz = "sample_tz"
			RIXS_sampleAlignement_selectOnClic(sample_tz)
			break
	endswitch

	return 0  
End





function RIXS_sampleAlignement_selectOnClic(sample)
	string sample

	variable checkTEYvalue, checkTFYvalue
	

// Vérif si au moins une checkBox tx et/ou tz est sélectionnée	
	variable checkTEYvalue_tx, checkTFYvalue_tx
	controlInfo /W=RIXSsampleAlignementPanel checkTEYsampleAlignement_tx ; checkTEYvalue_tx = V_Value
	controlInfo /W=RIXSsampleAlignementPanel checkTFYsampleAlignement_tx ; checkTFYvalue_tx = V_Value
	
	if (checkTEYvalue_tx == 0 && checkTFYvalue_tx == 0)
	
		DoAlert /T="!!! Checkbox !!!" 0, "Sample tx --> Checkbox TEY and TFY unchecked. Check at least one of them"
		return 0
	endif
	
	
	variable checkTEYvalue_tz, checkTFYvalue_tz
	controlInfo /W=RIXSsampleAlignementPanel checkTEYsampleAlignement_tz ; checkTEYvalue_tz = V_Value
	controlInfo /W=RIXSsampleAlignementPanel checkTFYsampleAlignement_tz ; checkTFYvalue_tz = V_Value
	
	if (checkTEYvalue_tz == 0 && checkTFYvalue_tz == 0)
	
		DoAlert /T="!!! Checkbox !!!" 0, "Sample tz --> Checkbox TEY and TFY unchecked. Check at least one of them"
		return 0
	endif
	
	
// Make d'une bufferWave et d'une selBufferWave 	réceptrices
	WAVE /T sampleAlignement_tx	= root:RIXS:Variables:sampleAlignement_tx 
	WAVE 	selSampleAlignement_tx	= root:RIXS:Variables:selSampleAlignement_tx
	WAVE /T sampleAlignement_tz	= root:RIXS:Variables:sampleAlignement_tz 
	WAVE 	selSampleAlignement_tz	= root:RIXS:Variables:selSampleAlignement_tz
	
	string nomGraphe
	
	strSwitch(sample)
	
		case "sample_tx" :
		
			Make /T/O/N=(numpnts(sampleAlignement_tx)) root:RIXS:Variables:bufferWave = sampleAlignement_tx
			WAVE /T bufferWave = root:RIXS:Variables:bufferWave
			Make /O/N=(numpnts(selSampleAlignement_tx)) root:RIXS:Variables:selBufferWave = selSampleAlignement_tx
			WAVE selBufferWave = root:RIXS:Variables:selBufferWave
			
			nomGraphe = "RIXSsampleAlignementPanel#visu_tx"
			checkTEYvalue = checkTEYvalue_tx
			checkTFYvalue = checkTFYvalue_tx
			break
			
		case "sample_tz" :
		
			Make /T/O/N=(numpnts(sampleAlignement_tz)) root:RIXS:Variables:bufferWave = sampleAlignement_tz
			WAVE /T bufferWave = root:RIXS:Variables:bufferWave
			Make /O/N=(numpnts(selSampleAlignement_tz)) root:RIXS:Variables:selBufferWave = selSampleAlignement_tz
			WAVE selBufferWave = root:RIXS:Variables:selBufferWave
			
			nomGraphe = "RIXSsampleAlignementPanel#visu_tz"
			checkTEYvalue = checkTEYvalue_tz
			checkTFYvalue = checkTFYvalue_tz
			break
	endSwitch
		

	SVAR dataPathFile = root:RIXS:Variables:dataPathFile
		
	WAVE rgb_R = root:RIXS:Variables:rgb_R
	WAVE rgb_G = root:RIXS:Variables:rgb_G
	WAVE rgb_B = root:RIXS:Variables:rgb_B
	variable taille_list 
	variable FileID, GroupID	
	string previous_DF
	variable i, j, k
	string groupePrincipal, dataSets_list, dataSetAttr_List, listItem, cible, listeCibles, tangoAddress, listeWave
	
	variable goFrom, charComp
	string keithley = "data_02"
	string counter = "data_03"
	string trajectory = "trajectory_1_1"
	
	
	
	// Placement dans le bon Folder selon tx ou tz
	previous_DF = getDataFolder(1) //; print "previous_DF -->", previous_DF
	strSwitch(sample)
	
		case "sample_tx" :
			
			setDataFolder root:RIXS:panels:sampleAlignementFolder:sample_tx
			break


		case "sample_tz" :
		
			setDataFolder root:RIXS:panels:sampleAlignementFolder:sample_tz
			break
	endSwitch	
	
	
	// CHARGEMENT des fichiers.nxs sélectionnés à chaque shift-clic
	for(i = 0 ; i <= numpnts(selBufferWave) - 1 ; i += 1)
	
		if(selBufferWave[i] == 1)
		
			cible = bufferWave[i] ; print "cible -->", cible
			
				HDF5OpenFile /R FileID as dataPathFile + cible
				
				HDF5Dump /A /ATTR = 0 /Q dataPathFile + cible				// Récupération du nom du groupe principal 
				goFrom = strSearch(S_HDF5Dump, "\"scan", 0)
				j = 1
					do
					
						charComp = cmpstr(S_HDF5Dump[goFrom + j], "\"")
						j += 1
					while(charComp != 0)
				groupePrincipal = S_HDF5Dump[goFrom + 1, goFrom + j-2] 
				
				HDF5OpenGroup FileID, groupePrincipal + "/scan_data", GroupID
				
				HDF5ListGroup FileID, groupePrincipal + "/scan_data" ; dataSets_list = S_HDF5ListGroup
				
				HDF5ListAttributes GroupID, stringFromList(0, dataSets_list, ";")		// Récupération 1er elt de la DataSetList pour l'étape d'après (récupération adresse tango)

				// Récupération de l'adresse Tango (celle correspondant au 1er élément de la dataSetsList)
				HDF5Dump /A = groupePrincipal + "/scan_data/" + stringFromList(0, dataSets_list, ";") + "/long_name" /Q dataPathFile + cible 
				
					goFrom = strsearch(S_HDF5Dump, "i14-m", 0)
					j = 0
					do
						charComp = cmpstr(S_HDF5Dump[goFrom + j], "\"")
						j += 1  
					while(charComp != 0)
					tangoAddress = S_HDF5Dump[goFrom, goFrom + j - 2]

				 
					HDF5LoadData/O/Q/VAR=0/N=$(groupePrincipal + "_" + keithley) GroupID, keithley
					HDF5LoadData/O/Q/VAR=0/N=$(groupePrincipal + "_" + counter) GroupID, counter
					HDF5LoadData/O/Q/VAR=0/N=$(groupePrincipal + "_" + trajectory) GroupID, trajectory
				

				HDF5CloseGroup	GroupID
				HDF5CloseFile FileID					
					
		else
				
				cible = bufferWave[i] ; print "cible -->", cible
	
					// Récup du groupePrincipal 
				HDF5OpenFile /R FileID as dataPathFile + cible			// Chargement du fichier.nxs[i]
									
				HDF5Dump /A /ATTR = 0 /Q dataPathFile + cible				
				goFrom = strSearch(S_HDF5Dump, "\"scan", 0)
				j = 1
					do
						
						charComp = cmpstr(S_HDF5Dump[goFrom + j], "\"")
						j += 1
					while(charComp != 0)
					groupePrincipal = S_HDF5Dump[goFrom + 1, goFrom + j-2]
						
				HDF5CloseFile FileID
					
				removeFromGraph /Z /W=$nomGraphe $(groupePrincipal + "_" + 	keithley)
				killWaves /Z $(groupePrincipal + "_" + 	keithley)
				
				removeFromGraph /Z /W=$nomGraphe $(groupePrincipal + "_" + 	counter)
				killWaves /Z $(groupePrincipal + "_" + 	counter)
				
				removeFromGraph /Z /W=$nomGraphe $(groupePrincipal + "_" + 	trajectory)
				killWaves /Z $(groupePrincipal + "_" + 	trajectory)
		
		endif
	endfor
//	
//	
	
	print "nomGraphe -->", nomGraphe
	
	// Affichage data_02,  data_03 = f(trajectory_1_1)
	// graph : RIXSbeamlineAlignementPanel#visu_tx
	string tracesList = traceNameList(nomGraphe, ";", 1)
	string wavesList_keithley = waveList("*data_02", ";", "")
	string wavesList_counter = waveList("*data_03", ";", "")
	string wavesList_trajectory = waveList("*trajectory*", ";", "")
	

	// On éliminine les waves précédentes
	for (i = 0 ; i <= itemsInList(tracesList, ";") - 1  ; i += 1)
	
		removeFromGraph /Z /W=$nomGraphe $stringFromList(i, tracesList, ";")
	endfor
	



	// Plot des waves keithley
	if (checkTEYvalue == 1)
		for (i = 0 ; i <= itemsInList(wavesList_keithley, ";") - 1  ; i += 1)
		
			appendToGraph /W=$nomGraphe /C=(rgb_R[i], rgb_G[i], rgb_B[i]) /R $stringFromList(i, wavesList_keithley, ";")
			ModifyGraph /W=$nomGraphe axRGB(right)=(65535,0,0), tlblRGB(right)=(65535,0,0), alblRGB(right)=(65535,0,0)
		endfor
	endif
	

	// Plot des waves counter
	if (checkTFYvalue == 1)
		for (i = 0 ; i <= itemsInList(wavesList_counter, ";") - 1  ; i += 1)
		
			appendToGraph /W=$nomGraphe /C=(rgb_R[i], rgb_G[i], rgb_B[i]) /L $stringFromList(i, wavesList_counter, ";")
			ModifyGraph /W=$nomGraphe lStyle($stringFromList(i, wavesList_counter, ";")) = 3	
			ModifyGraph /W=$nomGraphe axRGB(left)=(16385,28398,65535), tlblRGB(left)=(16385,28398,65535), alblRGB(left)=(16385,28398,65535)
		endfor
	endif


		
	
	legend /W=$nomGraphe /C /N=legende	
	Label /Z /W=$nomGraphe left "\\Z14Counter01 (..._data_03)"
	Label /Z /W=$nomGraphe right "\\Z14Keithley (..._data_02)"
	Label /Z /W=$nomGraphe bottom "\\Z14Photon energy (eV))"
	
	
	// Ménage buffer
	killWaves /Z bufferWave, selBufferWave
	
	setDataFolder previous_DF	

	return 0 
end











function btnProc_closeRIXSsampleAlignementPanel(ctrlName) : ButtonControl
	String ctrlName

	killWindow RIXSsampleAlignementPanel

	return 0
end
















