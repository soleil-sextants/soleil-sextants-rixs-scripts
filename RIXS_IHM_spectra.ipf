#pragma TextEncoding = "MacRoman"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.



function RIXSspectraWindow()

	doWindow /F RIXSspectraPanel
	if(V_flag == 1)
	
		return 0
	endif

	PauseUpdate; Silent 1		// building window...
	NewPanel /N=RIXSspectraPanel /W=(247,114,1140,606) as "Data"
	ModifyPanel fixedSize = 1
	
	SetDrawLayer UserBack
	SetDrawEnv fsize= 14
	DrawText 595,39,"nxs Files"	
	
	display /N=visu /HOST=RIXSspectraPanel /W=(22,43,582,443) 

	SetVariable setvarDataPathSpectra,pos={25,12},size={558,16},title="Path : ", proc=SetVarProc_onEnterDataPath, win=RIXSspectraPanel
	SetVariable setvarDataPathSpectra,limits={-inf,inf,0}, value = root:RIXS:Variables:dataPathFile,fSize=12
	
	ListBox listRIXSspectraScans,pos={593,43},size={283,399},proc=ListBoxProc_RIXSspectraScans, win=RIXSspectraPanel, mode=2
	ListBox listRIXSspectraScans, listWave = root:RIXS:Variables:nxsScansFiles
	ListBox listRIXSspectraScans, selWave = root:RIXS:Variables:selNxsScansFiles
	
	PopupMenu popupCallSpectraPanelType,pos={590.00,459.00},size={102.00,23.00},proc=PopMenuProc_spectraType,title="Spectra type"
	PopupMenu popupCallSpectraPanelType,mode=0,value= #"\"Beamline alignement;X-Ray absorption;Sample alignement;RIXS;All\""
	
	Button buttonCloseRIXSspectraPanel,pos={781.00,455.00},size={96.00,23.00},proc=btnProc_closeRIXSspectraPanel,title="Close"

	return 0
end



Function ListBoxProc_RIXSspectraScans(lba) : ListBoxControl
	STRUCT WMListboxAction &lba

	Variable row = lba.row
	Variable col = lba.col
	WAVE/T/Z listWave = lba.listWave
	WAVE/Z selWave = lba.selWave

	SVAR nxsSelected = root:RIXS:Variables:nxsSelected

	switch( lba.eventCode )
		case -1: // control being killed
			break
		case 1: // mouse down
			break
			
		case 2: // mouse up
			break			
			
		case 3: // double click
			break
	
		case 4: // cell selection
		
				if(row >= dimSize(listWave, 0))	// Si on clic sur un espace blanc de la listBox --> Evite retour d�bogueur
					Break
				endif
				
				nxsSelected = listWave[row] ; print "ListBoxProc_RIXSspectraScans(lba) : nxsSelected --> ", nxsSelected
				RIXS_loadNXS()
				//RIXS_getGroup(nxsSelected)
			break
		
		case 5: // cell selection plus shift key
			break
	
		case 6: // begin edit
			break
		
		case 7: // finish edit
			break
		
		case 13: // checkbox clicked (Igor 6.2 or later)
			break
	endswitch

	return 0
End




Function PopMenuProc_spectraType(ctrlName,popNum,popStr) : PopupMenuControl
	String ctrlName
	Variable popNum
	String popStr
	
	SVAR spectrumType = root:RIXS:Variables:spectrumType

	strswitch(popStr)
	
		case "Beamline alignement" :
			
			spectrumType = "nothing"		// Pour gestion abcisse chez RIXS_BeamlineAlignement_selectOnClic()
			RIXS_beamlineAlignement_DLG()
			break
			
		case "X-Ray absorption" :
			RIXSxasDLG()
			break
			
		case "Sample alignement" :
			RIXS_sampleAlignement_DLG()
			break
			
		case "RIXS" :
			RIXSrixsSpectra()
			break 
			
//		case "All" :
//			RIXS_beamlineAlignement_DLG()
//			RIXSxasDLG()
//			RIXSsampleTxDLG()
//			RIXSrixsSpectra()
//			break
			
	endswitch

	return 0

End









Function btnProc_closeRIXSspectraPanel(ctrlName) : ButtonControl
	String ctrlName
	
	killWindow RIXSspectraPanel
	
//	setDataFolder root:
//	killWaves /A
//	
//	killDataFolder root:wavesStock

	return 0
End














