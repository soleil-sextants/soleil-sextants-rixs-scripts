#pragma TextEncoding = "MacRoman"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.




_______________________________________________________________________________



										RIXS  /  Infos probl�mes surgissants


_______________________________________________________________________________





function RIXS_problemes()

	string previous_df = getdatafolder(1)
		
		// Variables probl�mes
		setDataFolder root:RIXS:infos_Problemes
		
		String /G unOpenFilesList		// Fichiers que HDF5OpenFile n'arrive pas � ouvrir
		String /G missGroupFilesList	// Fichiers � qui il manque le groupe SEXSTANTS ou scan_data
		String /G groupeEssentiel = "SEXTANTS;scan_data;"
		
		setDataFolder previous_df

	return 0
end