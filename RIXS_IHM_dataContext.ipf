#pragma TextEncoding = "MacRoman"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.






function RIXSedgeWindow()
	
	doWindow /F RIXSedgePanel
	if(V_flag == 1)
	
		return 0
	endif
		
	PauseUpdate; Silent 1		// building window...
	NewPanel /N=RIXSedgePanel /W=(166,48,878,537) as "Context data"
	//ModifyPanel fixedSize = 1
	
	SetDrawLayer UserBack
	SetDrawEnv /W=RIXSedgePanel fsize= 14
	DrawText /W=RIXSedgePanel 450,85,"Contextual Data"
	SetDrawEnv /W=RIXSedgePanel fsize= 14
	DrawText /W=RIXSedgePanel 128,86,"nxs Files"
	
	
	SetVariable setvarDataPathEdge,pos={18,25},size={622,16},title="Path ", proc=SetVarProc_onEnterDataPath, win=RIXSedgePanel
	SetVariable setvarDataPathEdge,limits={-inf,inf,0}, value = root:RIXS:Variables:dataPathFile,fSize=12
	
	// listBox : liste des scan_xxx.nxs
	ListBox listRIXSedgePanelScans,pos={17,96},size={280.00,342.00}	,proc=ListBoxProc_RIXSedgeScans, win=RIXSedgePanel, mode=2
	ListBox listRIXSedgePanelScans, listWave = root:RIXS:Variables:nxsScansFiles
	ListBox listRIXSedgePanelScans, selWave = root:RIXS:Variables:selNxsScansFiles	
	
	// listBox : meta donn�es (affichage apr�s s�lection d'un scan_xxx.nxs
	ListBox listRIXSedgePanelMetaData,pos={306,96},size={388.00,340.00}, win=RIXSedgePanel
	ListBox listRIXSedgePanelMetaData, listWave = root:RIXS:Variables:scanMetaDataWave
	ListBox listRIXSedgePanelMetaData, selWave = root:RIXS:Variables:selScanMetaDataWave

	
	Button buttonCloseMainWindow,pos={566.00,455.00},size={129,23},proc=ButtonProc_closeMainWindow,title="Close"
	

// checkBox mises en sommeil selon de cdc d'Alessandro (Eliminer les checkBox XAS, ..., RIXS)
//
//	CheckBox checkMainWindowXAS,pos={732,56},size={39,14},title="XAS"
//	CheckBox checkMainWindowXAS,value= 0, proc = CheckProc_callSpecificPanel
//	
//	CheckBox checkMainWindowSampleTx,pos={732,81},size={64,14},title="Sample tx"
//	CheckBox checkMainWindowSampleTx,value= 0, proc = CheckProc_callSpecificPanel
//	
//	CheckBox checkMainWindowSampleTz,pos={732,110},size={64,14},title="Sample tz"
//	CheckBox checkMainWindowSampleTz,value= 0, proc = CheckProc_callSpecificPanel
//	
//	CheckBox checkMainWindowSampleEnergy,pos={732,139},size={51,14},title="Energy"
//	CheckBox checkMainWindowSampleEnergy,value= 0, proc = CheckProc_callSpecificPanel
//	
//	CheckBox checkMainWindowSampleRIXS,pos={732,166},size={43,14},title="RIXS"
//	CheckBox checkMainWindowSampleRIXS,value= 0, proc = CheckProc_callSpecificPanel
	
	return 0
end




Function SetVarProc_onEnterDataPath(ctrlName,varNum,varStr,varName) : SetVariableControl
	String ctrlName
	Variable varNum
	String varStr
	String varName
	
	saveRIXSdlgState()			// Enregistrement sur DD de l'�tat  de l'IHM RIXS (en l'�tat du dataPathFile)
	RIXS_purge()				// Effacement des folders de l'analyse de donn�es en cours
	RIXS_chargementNXS()		// Chargement de nouveau fichiers.nxs pour une nouvelle analyse
	
	return 0
End




Function ListBoxProc_RIXSedgeScans(lba) : ListBoxControl
	STRUCT WMListboxAction &lba

	Variable row = lba.row
	Variable col = lba.col
	WAVE/T/Z listWave = lba.listWave
	WAVE/Z selWave = lba.selWave
	
	SVAR nxsSelected = root:RIXS:Variables:nxsSelected

	switch( lba.eventCode )
		case -1: // control being killed
			break
		case 1: // mouse down
			break
		case 3: // double click
			break
		case 4: // cell selection
		
				if(row >= dimSize(listWave, 0))	// Si on clic sur un espace blanc de la listBox --> Evite retour d�bogueur
					Break
				endif
				
				nxsSelected = listWave[row] //; print "nxsSelected --> ", nxsSelected
				RIXS_loadNXS()
			break
		case 5: // cell selection plus shift key
			break
		case 6: // begin edit
			break
		case 7: // finish edit
			break
		case 13: // checkbox clicked (Igor 6.2 or later)
			break
	endswitch

	return 0
End




_________________________________________________________________________________________________________________________________

fonction CheckProc_callSpecificPanel(ctrlName,checked) relative aux checkbox XAS, ..., RIXS mises en sommeil selon cdc Alessandro

Appels � la fct : RIXSmetaDataTargetWindow de RIXS_metaDataTarget.ipf  donc fichier en sommeil lui aussi
_________________________________________________________________________________________________________________________________
Function CheckProc_callSpecificPanel(ctrlName,checked) : CheckBoxControl
	String ctrlName
	Variable checked
	
	strSwitch(ctrlName)
	
		case "checkMainWindowXAS" :

			controlInfo /W = RIXSedgePanel checkMainWindowXAS
			if (V_Value == 0)
			
				doWindow /K $"check_MainWindowXAS"		// On ferme
			else
			
				RIXSmetaDataTargetWindow("check_MainWindowXAS", "XASFiles", "XAS")
			endif			
		break


		case "checkMainWindowSampleTx" :

			controlInfo /W = RIXSedgePanel checkMainWindowSampleTx
			if (V_Value == 0)
			
				doWindow /K $"check_MainWindowSampleTx"		// On ferme
			else
			
				RIXSmetaDataTargetWindow("check_MainWindowSampleTx", "Sample_txFiles", "SampleTx")
			endif					
		break
			
			
		case "checkMainWindowSampleTz" :

			controlInfo /W = RIXSedgePanel checkMainWindowSampleTz
			if (V_Value == 0)
			
				doWindow /K $"check_MainWindowSampleTz"		// On ferme
			else
			
				RIXSmetaDataTargetWindow("check_MainWindowSampleTz", "Sample_tzFiles", "SampleTz")
			endif							
		break
		
			
		case "checkMainWindowSampleEnergy" :
			
			controlInfo /W = RIXSedgePanel checkMainWindowSampleEnergy
			if (V_Value == 0)
			
				doWindow /K $"check_MainWindowSampleEnergy"		// On ferme
			else
			
				RIXSmetaDataTargetWindow("check_MainWindowSampleEnergy", "MonoFiles", "Energie mono")
			endif					
		break
			
			
		case "checkMainWindowSampleRIXS" :
		
			controlInfo /W = RIXSedgePanel checkMainWindowSampleRIXS
			if (V_Value == 0)
			
				doWindow /K $"check_MainWindowSampleRIXS"		// On ferme
			else
			
				RIXSmetaDataTargetWindow("check_MainWindowSampleRIXS", "RIXSfiles", "RIXS")
			endif							
		break
			
		endSwitch
	
	return 0

End







Function ButtonProc_closeMainWindow(ctrlName) : ButtonControl
	String ctrlName

	killWindow RIXSedgePanel
	
	return 0
End





