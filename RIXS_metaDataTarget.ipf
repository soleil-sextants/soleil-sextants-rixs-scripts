#pragma rtGlobals=3		// Use modern global access method and strict wave access.


_______________________________________________________________________________________________________________________________________

Fichier RIXS_metaDataTarget.ipf en sommeil suite à la mise en commentaire des checkBox XAS, ..., RIXS dans la fonction RIXSedgeWindow()

du fichier RIXS_IHM_edge.ipf selon cdc Alessandro
_______________________________________________________________________________________________________________________________________


function RIXSmetaDataTargetWindow(nomFenetre, typeSelectionne, titreFenetre)
	string nomFenetre, typeSelectionne, titreFenetre
	
	PauseUpdate; Silent 1		// building window...
	NewPanel /N=$nomFenetre /W=(262,154,835,485) as titreFenetre
	
	ListBox listTargetType,pos={10,13},size={257,307},proc=ListBoxProc_XAS
	ListBox listTargetType, listWave = root:RIXS:Variables:$typeSelectionne
	ListBox listTargetType, selWave = root:RIXS:Variables:$("sel" + typeSelectionne)
	
	ListBox listMetaDataTarget,pos={274,12},size={287,307}
	ListBox listMetaDataTarget, listWave=root:RIXS:Variables:scanMetaDataWave
	ListBox listMetaDataTarget, selWave=root:RIXS:Variables:selScanMetaDataWave,mode= 2
	
	return 0
end




Function ListBoxProc_XAS(lba) : ListBoxControl
	STRUCT WMListboxAction &lba

	Variable row = lba.row
	Variable col = lba.col
	WAVE/T/Z listWave = lba.listWave
	WAVE/Z selWave = lba.selWave
	
	SVAR nxsSelected = root:RIXS:Variables:nxsSelected

	switch( lba.eventCode )
		case -1: // control being killed
			break
		case 1: // mouse down
			break
		case 3: // double click
			break
		
		case 4: // cell selection

			nxsSelected = listWave[row] //; print "nxsSelected --> ", nxsSelected
			RIXS_loadNXS()		
			break
		
		case 5: // cell selection plus shift key
			break
		case 6: // begin edit
			break
		case 7: // finish edit
			break
		case 13: // checkbox clicked (Igor 6.2 or later)
			break
	endswitch

	return 0
End

