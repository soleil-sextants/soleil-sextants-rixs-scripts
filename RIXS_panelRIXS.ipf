﻿#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.




function RIXSrixsSpectra()


	doWindow /F RIXSrixsSpectraPanel
	if(V_flag == 1)
	
		return 0
	endif
	
	
	PauseUpdate; Silent 1		// building window...
	NewPanel /N=RIXSrixsSpectraPanel /W=(334,292,1227,784) as "RIXS Spectra : RIXS Spectra"
	ModifyPanel fixedSize=1
	
	display /N=visu /HOST=RIXSrixsSpectraPanel /W=(22,43,582,443)
	
	ListBox listRIXSrixsSpectraPanel,pos={594.00,67.00},size={282.00,353.00}
	ListBox listRIXSrixsSpectraPanel,mode= 4
	
	PopupMenu popupRIXSrixsSpectra,pos={592.00,41.00},size={106.00,23.00},proc=PopMenuProc_beamlineAlignement,title="Type de scan"
	PopupMenu popupRIXSrixsSpectra,mode=0,value= #"\"Energy;Positions Mono H slits;Position RIXS V slits\""
	
	SetVariable setvar_RIXSrixsSpectraType,pos={703.00,40.00},size={172.00,18.00},title=" "
	SetVariable setvar_RIXSrixsSpectraType,fSize=12,frame=0
	SetVariable setvar_RIXSrixsSpectraType,limits={-inf,inf,0},value= root:RIXS:Variables:spectraType
	
	Button btnPlotRixsSpectraPanelSelection,pos={592.00,424.00},size={126.00,21.00},title="Plot selection"
	Button btnRemoveRixsSpectraPanelSelection,pos={750.00,424.00},size={126.00,21.00},title="Remove selection"
	
	Button buttonCloseRIXSxRayAbsorptionPanel,pos={748.00,455.00},size={129.00,23.00},proc=btnProc_closeRIXSrixsSpectraPanel,title="Close"	
	
	
	
	return 0
end




function btnProc_closeRIXSrixsSpectraPanel()



	return 0
end











