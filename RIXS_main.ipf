#pragma TextEncoding = "MacRoman"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.


Last - 07 / 03 / 2021
_______________________________________________________________________________

                       
                       RIXS / Donn�es HDF5 / Traitements   

_______________________________________________________________________________


// Accès programme
menu "RIXS"

	"RIXS Data", RIXS_init()
	"-"
	"RIXS --> Spectra", RIXS_init()
	"RIXS --> Edge", RIXS_init()
	"RIXS --> Central Infos", RIXS_init()
	"-"
	"Edit --> file.nxs - type", Edit /N=editScanType /K=1 /W=(592,181,1274,797) root:RIXS:Variables:nxsScansFile_fileType
	"-"
	"Save this session as Experiment", saveExperiment
	"-"
	"Test", RIXS_getNXS("scan_003.nxs")
	"-"
	"Hide Procedures", HideProcedures
	"Show RIXS Windows", RIXS_showRIXSwindows()
	"Hide RIXS Windows", RIXS_hideRIXSwindows()
	"-"
	"Close RIXS", closeRIXSapp()
end

/Users/stephbac/Desktop/Desk/prgm_Igor/Alessandro_RIXS/prjRIXS
_____________________________________________________________________________________________
// D�pendances iMac

//#include "hd:Users:stephbac:Desktop:Desk:prgm_Igor:Alessandro_RIXS:prjRIXS:RIXS_IHM_dataContext"
//#include "hd:Users:stephbac:Desktop:Desk:prgm_Igor:Alessandro_RIXS:prjRIXS:RIXS_IHM_spectra"
//#include "hd:Users:stephbac:Desktop:Desk:prgm_Igor:Alessandro_RIXS:prjRIXS:RIXS_IHM_centralInfo"
//
//#include "hd:Users:stephbac:Desktop:Desk:prgm_Igor:Alessandro_RIXS:prjRIXS:RIXS_panel_beamlineAlignement"
//#include "hd:Users:stephbac:Desktop:Desk:prgm_Igor:Alessandro_RIXS:prjRIXS:RIXS_panelXAS"
//#include "hd:Users:stephbac:Desktop:Desk:prgm_Igor:Alessandro_RIXS:prjRIXS:RIXS_panel_sampleAlignement"
//#include "hd:Users:stephbac:Desktop:Desk:prgm_Igor:Alessandro_RIXS:prjRIXS:RIXS_panelRIXS"
//
//#include "hd:Users:stephbac:Desktop:Desk:prgm_Igor:Alessandro_RIXS:prjRIXS:RIXS_variables"
//#include "hd:Users:stephbac:Desktop:Desk:prgm_Igor:Alessandro_RIXS:prjRIXS:RIXS_utilities"
//#include "hd:Users:stephbac:Desktop:Desk:prgm_Igor:Alessandro_RIXS:prjRIXS:RIXS_getHDF5"
//#include "hd:Users:stephbac:Desktop:Desk:prgm_Igor:Alessandro_RIXS:prjRIXS:RIXS_metaDataTarget"
//#include "hd:Users:stephbac:Desktop:Desk:prgm_Igor:Alessandro_RIXS:prjRIXS:RIXS_infosProblemes"
//#include "hd:Users:stephbac:Desktop:Desk:prgm_Igor:Alessandro_RIXS:prjRIXS:RIXS_devTest"


---------------------------------------------------------------------------------------------
// D�pendances PC

#include "D:prgms:Igor:Alessandro_RIXS:prjRIXS:RIXS_IHM_dataContext"
#include "D:prgms:Igor:Alessandro_RIXS:prjRIXS:RIXS_IHM_spectra"
#include "D:prgms:Igor:Alessandro_RIXS:prjRIXS:RIXS_IHM_centralInfo"

#include "D:prgms:Igor:Alessandro_RIXS:prjRIXS:RIXS_panel_beamlineAlignement"
#include "D:prgms:Igor:Alessandro_RIXS:prjRIXS:RIXS_panelXAS"
#include "D:prgms:Igor:Alessandro_RIXS:prjRIXS:RIXS_panel_sampleAlignement"
#include "D:prgms:Igor:Alessandro_RIXS:prjRIXS:RIXS_panelRIXS"

#include "D:prgms:Igor:Alessandro_RIXS:prjRIXS:RIXS_variables"
#include "D:prgms:Igor:Alessandro_RIXS:prjRIXS:RIXS_utilities"
//#include "D:prgms:Igor:Alessandro_RIXS:prjRIXS:RIXS_getHDF5"
#include "D:prgms:Igor:Alessandro_RIXS:prjRIXS:RIXS_metaDataTarget"
#include "D:prgms:Igor:Alessandro_RIXS:prjRIXS:RIXS_infosProblemes"
#include "D:prgms:Igor:Alessandro_RIXS:prjRIXS:RIXS_devTest"
---------------------------------------------------------------------------------------------





// Cr�ation environnement programme
function RIXS_init()

	variable df_Exists = dataFolderExists("root:RIXS")		
	
	
	if (df_Exists == 0)		// 0 : n'existe pas

		// Creation Folders programme
		NewDataFolder root:RIXS
		NewDataFolder root:RIXS:Variables
		NewDataFolder root:RIXS:infos_Problemes
		NewDataFolder root:RIXS:buffer
		NewDataFolder root:RIXS:buffer:contextualData
		
		// Panels : beamlineAlignement		--> Energy, Mono H slits, RIXS V slits
		//        : XRayAbsorption			--> XAS
		//        : sampleAlignement		--> Sample-tx, Sample-tz
		//        : RIXS						--> RIXS  
		NewDataFolder root:RIXS:panels
		NewDataFolder root:RIXS:panels:beamlineAlignementFolder
		NewDataFolder root:RIXS:panels:XRayAbsorptionFolder
		NewDataFolder root:RIXS:panels:sampleAlignementFolder
		NewDataFolder root:RIXS:panels:sampleAlignementFolder:sample_tx
		NewDataFolder root:RIXS:panels:sampleAlignementFolder:sample_tz
		NewDataFolder root:RIXS:panels:RIXSFolder
		
		
		string previous_df = getdatafolder(1)
		setDataFolder root:RIXS:Variables

			// Chargement du dernier path aux donn�es saisi par l'utilisateur
			LoadWave /A/W/O/J/K=2/Q "D:prgms:Igor:Alessandro_RIXS:prjRIXS:RIXSdlgState.txt"
			
			// Chargement de la palette pour les courbes 1D
			LoadWave /A/W/O/Q/J/K=1  "D:prgms:Igor:Alessandro_RIXS:prjRIXS:palette.txt"
			
		setDataFolder previous_df
		
			// Variables programme
			RIXS_variables()
			
			// Variables probl�mes
			RIXS_problemes()	
	endif


	// Panneau o� s'affiche tous les fichiers.nxs. S�lection d'un fichier --> affichage Meta Data
	doWindow /F RIXSedgePanel
	if (V_flag == 0)
	
		RIXSedgeWindow()
	endif
	
	
	// Panneau o� les fichiers.nxs sont tri�s par titre. S�lection d'un fichier --> affichage Meta Data & courbe (data)
	doWindow /F RIXScentralPanel
	if (V_flag == 0)
	
		RIXScentralInfoWindow()
	endif
	
	
	// Panneau o� s'affiche tous les fichiers.nxs. S�lection d'un fichier --> affichage courbe (data)
	doWindow /F RIXSspectraPanel
	if (V_flag == 0)	// Pas de fen�tre ouverte
	
		RIXSspectraWindow()
		//AppendToGraph /W=RIXSspectraPanel#visu root:RIXS:Garbage:bruit1, root:RIXS:Garbage:bruit2, root:RIXS:Garbage:bruit3
	endif
	
	return 0
end





// Close App : élimination des fenêtres et dataFolder
function closeRIXSapp()

//	DoAlert /T="Exit RIXS" 1, "Sure to exit RIXS ?"
//	if (V_flag == 1)
		
		saveRIXSdlgState()
		
		killWindow RIXSedgePanel				// avec /Z pour Igor8
		killWindow RIXSspectraPanel			// avec /Z pour Igor8
		
		doWindow /K RIXScentralPanel
	
		killDataFolder /Z root:RIXS
//	endif

	return 0
end




function RIXS_showRIXSwindows()


	doWindow /Z /HIDE=0 RIXScentralPanel
	doWindow /Z /HIDE=0 RIXSspectraPanel
	doWindow /Z /HIDE=0 RIXSedgePanel
	doWindow /Z /HIDE=0 RIXSbeamlineAlignementPanel

	//	WinList
	//	winName
	//	WinType

	return 0
end



function RIXS_hideRIXSwindows()


	doWindow /Z /HIDE=1 RIXScentralPanel
	doWindow /Z /HIDE=1 RIXSspectraPanel
	doWindow /Z /HIDE=1 RIXSedgePanel
	doWindow /Z /HIDE=1 RIXSbeamlineAlignementPanel

	//	WinList
	//	winName
	//	WinType

	return 0
end




function saveRIXSdlgState()

	WAVE /T RIXSdlgState = root:RIXS:Variables:RIXSdlgState
	SVAR dataPathFile = root:RIXS:Variables:dataPathFile ; RIXSdlgState[0] = dataPathFile
	
	Save /W/O/J/M="\r\n" RIXSdlgState as "D:prgms:Igor:Alessandro_RIXS:prjRIXS:RIXSdlgState.txt"
	
	return 0
end





