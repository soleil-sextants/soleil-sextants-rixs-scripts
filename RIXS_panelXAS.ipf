﻿#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.




function RIXSxasDLG()


	doWindow /F RIXSxasPanel
	if(V_flag == 1)
	
		return 0
	endif
	
	PauseUpdate; Silent 1		// building window...
	NewPanel /N=RIXSxasPanel /W=(285,246,1178,738) as "RIXS Spectra : XAS"
	ModifyPanel fixedSize=1
	
	display /N=visu /HOST=RIXSxasPanel /W=(22,43,582,443)
	
	// Affichage ou non des cursors
	CheckBox checkCursors,pos={594.00,42.00},size={86.00,16.00},proc=CheckProc_cursors,title="Cursors A & B"
	CheckBox checkCursors,value= 0	
	
	// Sélection pour affichage ou non de du "Total Electron Yield" et du "Total Fluorescence Yield"
	CheckBox checkTEY,pos={785.00,43.00},size={31.00,16.00},title="TEY",value= 1, proc=CheckProc_TEY_TFY
	CheckBox checkTFY,pos={842.00,43.00},size={31.00,16.00},title="TFY",value= 1, proc=CheckProc_TEY_TFY
	
	// Affichage / sélection des fichiers.nxs
	ListBox listRIXSxRayAbsorption,pos={594.00,67.00},size={282.00,214.00}
	ListBox listRIXSxRayAbsorption,mode= 4, proc=ListBoxProc_XRayAbsorption
	ListBox listRIXSxRayAbsorption , listWave = root:RIXS:Variables:XRayAbsorption
	ListBox listRIXSxRayAbsorption , selWave = root:RIXS:Variables:selXRayAbsorption
	
	// Données contextuelles
	SetVariable setvarPolarization,pos={709.00,292.00},size={166.00,19.00},bodyWidth=100,title="Polarization"
	SetVariable setvarPolarization,limits={-inf,inf,0}, value = root:RIXS:Variables:polarization, disable=2
	
	SetVariable setvarSampleRz,pos={716.00,316.00},size={159.00,19.00},bodyWidth=100,title="Sample Rz"
	SetVariable setvarSampleRz,limits={-inf,inf,0}, value = root:RIXS:Variables:sample_Rz, disable=2
	
	SetVariable setvarSampleTx,pos={718.00,340.00},size={157.00,19.00},bodyWidth=100,title="Sample Tx"
	SetVariable setvarSampleTx,limits={-inf,inf,0}, value = root:RIXS:Variables:sample_Tx, disable=2
	
	SetVariable setvarSampleTs,pos={718.00,365.00},size={157.00,19.00},bodyWidth=100,title="Sample Ts"
	SetVariable setvarSampleTs,limits={-inf,inf,0}, value = root:RIXS:Variables:sample_Ts, disable=2
	
	SetVariable setvarSampleTz,pos={718.00,390.00},size={157.00,19.00},bodyWidth=100,title="Sample Tz"
	SetVariable setvarSampleTz,limits={-inf,inf,0}, value = root:RIXS:Variables:sample_Tz, disable=2
	
	// Pour normalisation et soustractions de data
	Button btnNormalisation,pos={592.00,424.00},size={85.00,21.00},proc=ButtonProc_Normalisation,title="Normalisation"
	Button btnDivisionA,pos={730.00,424.00},size={70.00,21.00},proc=ButtonProc_soustract_A,title="Divide A"
	Button btnSoustractB,pos={806.00,424.00},size={70.00,21.00},proc=ButtonProc_soustract_B,title="Soustract B"
	
	Button buttonCloseRIXSxasPanel,pos={748.00,455.00},size={129.00,23.00},proc=btnProc_closeRIXSxasPanel,title="Close"
			
	return 0
end



Function CheckProc_cursors(ctrlName,checked) : CheckBoxControl
	String ctrlName
	Variable checked
	
	controlInfo /W=RIXSxasPanel checkCursors
	
	if (V_Value == 0)
	
		HideInfo /W=RIXSxasPanel
	else
	
		ShowInfo /CP=0 /W=RIXSxasPanel		// /CP=0 ==> cursours A & B
	endif
End




Function CheckProc_TEY_TFY(ctrlName,checked) : CheckBoxControl
	String ctrlName
	Variable checked
	
	variable checkTEYvalue, checkTFYvalue
	controlInfo /W=RIXSxasPanel checkTEY ; checkTEYvalue = V_Value
	controlInfo /W=RIXSxasPanel checkTFY ; checkTFYvalue = V_Value
	
	if (checkTEYvalue == 0 && checkTFYvalue == 0)
	
		DoAlert /T="!!! Checkbox !!!" 0, "Checkbox TEY and TFY unchecked. Check at least one of them"
		CheckBox $ctrlName win = RIXSxasPanel, value= 1
		return 0
	endif
	
	WAVE 	selXRayAbsorption	= root:RIXS:Variables:selXRayAbsorption
	waveStats /Q selXRayAbsorption
	if (V_sum != 0)
	
		// Envoi pour nouveau remplissage graphe avec la la sélection en vigueur
		RIXS_XAS_selectOnClic()
	endif
		
	return 0
End




Function ListBoxProc_XRayAbsorption(lba) : ListBoxControl
	STRUCT WMListboxAction &lba

	Variable row = lba.row
	Variable col = lba.col
	WAVE/T/Z listWave = lba.listWave
	WAVE/Z selWave = lba.selWave
	
	
	
	SVAR spectraType = root:RIXS:Variables:spectraType
	variable i
	string selectionList
	

	switch( lba.eventCode )
		case -1: // control being killed
			break
		case 1: // mouse down
			break
		case 3: // double click
			break	
		case 4: // cell selection
			 break		
		
		case 5: // cell selection plus shift key
		
			RIXS_XAS_selectOnClic()
			break
		
		case 6: // begin edit
			break
		case 7: // finish edit
			break
		case 13: // checkbox clicked (Igor 6.2 or later)
			break
	endswitch

	return 0  
End




function RIXS_XAS_selectOnClic()

	variable checkTEYvalue, checkTFYvalue
	controlInfo /W=RIXSxasPanel checkTEY ; checkTEYvalue = V_Value
	controlInfo /W=RIXSxasPanel checkTFY ; checkTFYvalue = V_Value
	
	if (checkTEYvalue == 0 && checkTFYvalue == 0)
	
		DoAlert /T="!!! Checkbox !!!" 0, "Checkbox TEY and TFY unchecked. Check at least one of them"
		return 0
	endif
	
	
	SVAR dataPathFile = root:RIXS:Variables:dataPathFile

	WAVE /T XRayAbsorption	= root:RIXS:Variables:XRayAbsorption 
	WAVE 	selXRayAbsorption	= root:RIXS:Variables:selXRayAbsorption

	WAVE rgb_R = root:RIXS:Variables:rgb_R
	WAVE rgb_G = root:RIXS:Variables:rgb_G
	WAVE rgb_B = root:RIXS:Variables:rgb_B
	variable taille_list 
	variable FileID, GroupID	
	string previous_DF
	variable i, j, k
	string groupePrincipal, dataSets_list, dataSetAttr_List, listItem, cible, listeCibles, tangoAddress, listeWave
	
	variable goFrom, charComp
	string keithley = "data_02"
	string counter = "data_03"
	string trajectory = "trajectory_1_1"
	
	
	
	// CHARGEMENT des fichiers.nxs sélectionnés à chaque shift-clic
	previous_DF = getDataFolder(1) //; print "previous_DF -->", previous_DF
	setDataFolder root:RIXS:panels:XRayAbsorptionFolder	
	
	for(i = 0 ; i <= numpnts(selXRayAbsorption) - 1 ; i += 1)
	
		if(selXRayAbsorption[i] == 1)
		print "dans if"
			cible = XRayAbsorption[i]
			
				HDF5OpenFile /R FileID as dataPathFile + cible
				
				HDF5Dump /A /ATTR = 0 /Q dataPathFile + cible				// Récupération du nom du groupe principal 
				goFrom = strSearch(S_HDF5Dump, "\"scan", 0)
				j = 1
					do
					
						charComp = cmpstr(S_HDF5Dump[goFrom + j], "\"")
						j += 1
					while(charComp != 0)
				groupePrincipal = S_HDF5Dump[goFrom + 1, goFrom + j-2] 
				
				HDF5OpenGroup FileID, groupePrincipal + "/scan_data", GroupID
				
				HDF5ListGroup FileID, groupePrincipal + "/scan_data" ; dataSets_list = S_HDF5ListGroup
				
				HDF5ListAttributes GroupID, stringFromList(0, dataSets_list, ";")		// Récupération 1er elt de la DataSetList pour l'étape d'après (récupération adresse tango)

				// Récupération de l'adresse Tango (celle correspondant au 1er élément de la dataSetsList)
				HDF5Dump /A = groupePrincipal + "/scan_data/" + stringFromList(0, dataSets_list, ";") + "/long_name" /Q dataPathFile + cible 
				
					goFrom = strsearch(S_HDF5Dump, "i14-m", 0)
					j = 0
					do
						charComp = cmpstr(S_HDF5Dump[goFrom + j], "\"")
						j += 1  
					while(charComp != 0)
					tangoAddress = S_HDF5Dump[goFrom, goFrom + j - 2]

				 
					HDF5LoadData/O/Q/VAR=0/N=$(groupePrincipal + "_" + keithley) GroupID, keithley
					HDF5LoadData/O/Q/VAR=0/N=$(groupePrincipal + "_" + counter) GroupID, counter
					HDF5LoadData/O/Q/VAR=0/N=$(groupePrincipal + "_" + trajectory) GroupID, trajectory
				

				HDF5CloseGroup	GroupID
				HDF5CloseFile FileID					
					
		else
				print "dans else"
				cible = XRayAbsorption[i]
	
					// Récup du groupePrincipal 
				HDF5OpenFile /R FileID as dataPathFile + cible			// Chargement du fichier.nxs[i]
									
				HDF5Dump /A /ATTR = 0 /Q dataPathFile + cible				
				goFrom = strSearch(S_HDF5Dump, "\"scan", 0)
				j = 1
					do
						
						charComp = cmpstr(S_HDF5Dump[goFrom + j], "\"")
						j += 1
					while(charComp != 0)
					groupePrincipal = S_HDF5Dump[goFrom + 1, goFrom + j-2]
						
				HDF5CloseFile FileID
					
				removeFromGraph /Z /W=RIXSxasPanel#visu $(groupePrincipal + "_" + 	keithley)
				killWaves /Z $(groupePrincipal + "_" + 	keithley)
				
				removeFromGraph /Z /W=RIXSxasPanel#visu $(groupePrincipal + "_" + 	counter)
				killWaves /Z $(groupePrincipal + "_" + 	counter)
				
				removeFromGraph /Z /W=RIXSxasPanel#visu $(groupePrincipal + "_" + 	trajectory)
				killWaves /Z $(groupePrincipal + "_" + 	trajectory)
		
		endif
	endfor
	
	
	// Affichage data_02,  data_03 = f(trajectory_1_1)
	// graph : RIXSbeamlineAlignementPanel#visu
	string tracesList = traceNameList("RIXSxasPanel#visu", ";", 1) ; print "tracesList -->", tracesList
	string wavesList_keithley = waveList("*data_02", ";", "") ; print "wavesList_keithley -->", wavesList_keithley
	string wavesList_counter = waveList("*data_03", ";", "") ; print "wavesList_counter -->", wavesList_counter
	string wavesList_trajectory = waveList("*trajectory*", ";", "") ; print "wavesList_trajectory -->", wavesList_trajectory
	

	// On éliminine les waves précédentes
	for (i = 0 ; i <= itemsInList(tracesList, ";") - 1  ; i += 1)
	
		removeFromGraph /Z /W=RIXSxasPanel#visu $stringFromList(i, tracesList, ";")
	endfor
	
	
	// Plot des waves keithley
	if (checkTEYvalue == 1)
		for (i = 0 ; i <= itemsInList(wavesList_keithley, ";") - 1  ; i += 1)
		
			appendToGraph /W=RIXSxasPanel#visu /C=(rgb_R[i], rgb_G[i], rgb_B[i]) /R $stringFromList(i, wavesList_keithley, ";")
			ModifyGraph /W=RIXSxasPanel#visu axRGB(right)=(65535,0,0), tlblRGB(right)=(65535,0,0), alblRGB(right)=(65535,0,0)
		endfor
	endif
	

	// Plot des waves counter
	if (checkTFYvalue == 1)
		for (i = 0 ; i <= itemsInList(wavesList_counter, ";") - 1  ; i += 1)
		
			appendToGraph /W=RIXSxasPanel#visu /C=(rgb_R[i], rgb_G[i], rgb_B[i]) /L $stringFromList(i, wavesList_counter, ";")
			ModifyGraph /W=RIXSxasPanel#visu lStyle($stringFromList(i, wavesList_counter, ";")) = 3	
			ModifyGraph /W=RIXSxasPanel#visu axRGB(left)=(16385,28398,65535), tlblRGB(left)=(16385,28398,65535), alblRGB(left)=(16385,28398,65535)
		endfor
	endif


		
	
	legend /W=RIXSxasPanel#visu /C /N=legende	
	Label /Z /W=RIXSxasPanel#visu left "\\Z14Counter01 (..._data_03)"
	Label /Z /W=RIXSxasPanel#visu right "\\Z14Keithley (..._data_02)"
	Label /Z /W=RIXSxasPanel#visu bottom "\\Z14Photon energy (eV))"
	
	
	
	
	
	setDataFolder previous_DF	

	return 0 
end






Function ButtonProc_Normalisation(ctrlName) : ButtonControl
	String ctrlName


	string previous_DF = getDataFolder(1) 
	setDataFolder root:RIXS:panels:XRayAbsorptionFolder

		string wavesList_keithley = waveList("*data_02", ";", "") ; print "wavesList_keithley -->", wavesList_keithley
		string wavesList_counter = waveList("*data_03", ";", "") ; print "wavesList_counter -->", wavesList_counter
		string wavesList = wavesList_keithley + wavesList_counter ; print "wavesList -->", wavesList
		variable nbWaves = itemsInList(wavesList, ";")
		variable i
		
		for (i = 0 ; i <= nbWaves - 1 ; i++)
		
			WAVE DestWave = $stringFromList(i, wavesList, ";")
			
			waveStats /Q DestWave
			DestWave = DestWave - V_min
						
			waveStats /Q DestWave
			if (V_max != 0)
			
				DestWave = DestWave/V_max
			else
			
				DestWave = 1
			endif
		
		endfor
		
	setDataFolder previous_DF

	return 0
End




Function ButtonProc_soustract_A(ctrlName) : ButtonControl
	String ctrlName

	string previous_DF = getDataFolder(1) 
	setDataFolder root:RIXS:panels:XRayAbsorptionFolder	
	
		variable valeur_y = vcsr(A, "RIXSxasPanel#visu")
		WAVE targetWave = $csrWave(A, "RIXSxasPanel#visu")
		
		targetWave = targetWave/valeur_y

	return 0
End




Function ButtonProc_soustract_B(ctrlName) : ButtonControl
	String ctrlName

	string previous_DF = getDataFolder(1) 
	setDataFolder root:RIXS:panels:XRayAbsorptionFolder	
	
		variable valeur_y = vcsr(B, "RIXSxasPanel#visu")
		WAVE targetWave = $csrWave(B, "RIXSxasPanel#visu")
		
		targetWave = targetWave - valeur_y
		
	setDataFolder previous_DF	

	return 0
End





function btnProc_closeRIXSxasPanel(ctrlName) : ButtonControl
	String ctrlName

	killWindow RIXSxasPanel

	return 0
end




