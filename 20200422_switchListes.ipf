// imported by Alessandro

#pragma rtGlobals=1		// Use modern global access method.

Function Which_Scan(FileToOpen)		// this returns 1 se trova un dataset "actuator" o 0 se non trova l'actuator. L'actuator non  presente nelle acquisizioni CCD_Acquire()
String FileToOpen

// Result:
// 1 --> RIXS
// 2 --> XAS beamlineenergy/energy scan
// 3 --> tx-scan
// 4 --> tz-scan
// 5 --> smono-posx
// 6 --> srixs_gapz
// 7 --> mono/energy scan


variable var
	var=NaN


NVAR FileID	=:FileID	// Punta al File HDF5
NVAR GroupID=:GroupID	// Punta al gruppo

String NXSname
String GroupPath
Variable nxsnamelen

String Result_txt

Make/O/T/N=1 wave_txt


HDF5OpenFile /R FileID as FileToOpen
	HDF5Dump/A/Q/attr=0 FileToOpen		
		nxsnamelen= strsearch(S_HDF5Dump, "SEXTANTS", 0)-strsearch(S_HDF5Dump, "GROUP", 0)-40
		NXSname=S_HDF5Dump[strsearch(S_HDF5Dump, "GROUP", 0)+22,strsearch(S_HDF5Dump, "GROUP", 0)+22+nxsnamelen]

		GroupPath="/"+NXSname
		
		HDF5OpenGroup FileID, GroupPath, GroupID
			HDF5ListGroup /TYPE=1 GroupID, GroupPath
			
			if (stringmatch(S_HDF5ListGroup,"*scan_data*")==1)
				
				GroupPath="/"+NXSname+"/scan_data"
					HDF5OpenGroup FileID, GroupPath, GroupID
						
						HDF5ListAttributes /TYPE=1 GroupID, GroupPath			/// attributi del gruppo
						
						HDF5ListAttributes /TYPE=2 GroupID, "data_01"	

						HDF5ListGroup /TYPE=2 GroupID, GroupPath						

											HDF5LoadData/Q/O/A="long_name"/COMP={1, "value"}/VAR=1/N=wave_txt GroupID, "data_01"
											result_txt = wave_txt[0]
																							
												if (stringmatch(result_txt,"*princeton.rixs.1/image")==1)
														
													var=1
												else
												endif

										//
										if (stringmatch(S_HDF5ListGroup,"actuator_1_1*")==1)
																						
											HDF5LoadData/Q/O/A="long_name"/COMP={1, "value"}/VAR=1/N=wave_txt GroupID, "actuator_1_1"
											result_txt = wave_txt[0]
																							
												if (stringmatch(result_txt,"*beamlineenergy/energy")==1)
														
													var = 2
												else
												endif
										else
										endif			
												
										//
										if (stringmatch(S_HDF5ListGroup,"actuator_1_1*")==1)
																						
											HDF5LoadData/Q/O/A="long_name"/COMP={1, "value"}/VAR=1/N=wave_txt GroupID, "actuator_1_1"
											result_txt = wave_txt[0]
											
												if (stringmatch(result_txt,"*sample-tx*")==1)
														
													var = 3
												else
												endif
										else
										endif			
										
										//
										if (stringmatch(S_HDF5ListGroup,"actuator_1_1*")==1)
																						
											HDF5LoadData/Q/O/A="long_name"/COMP={1, "value"}/VAR=1/N=wave_txt GroupID, "actuator_1_1"
											result_txt = wave_txt[0]
												
												if (stringmatch(result_txt,"*sample-tz*")==1)
														
													var = 4
												else
												endif
										else
										endif																											

										//
										if (stringmatch(S_HDF5ListGroup,"actuator_1_1*")==1)
																						
											HDF5LoadData/Q/O/A="long_name"/COMP={1, "value"}/VAR=1/N=wave_txt GroupID, "actuator_1_1"
											result_txt = wave_txt[0]
																							
												if (stringmatch(result_txt,"*fent_h1*")==1)
													
													var = 5
												else
												endif
										else
										endif			
										
										//
										if (stringmatch(S_HDF5ListGroup,"actuator_1_1*")==1)
																						
											HDF5LoadData/Q/O/A="long_name"/COMP={1, "value"}/VAR=1/N=wave_txt GroupID, "actuator_1_1"
											result_txt = wave_txt[0]
											
												if (stringmatch(result_txt,"*srixs_gapz*")==1)
													
													var = 6
												else
												endif
										else
										endif													
										
										
										//
										if (stringmatch(S_HDF5ListGroup,"actuator_1_1*")==1)
																						
											HDF5LoadData/Q/O/A="long_name"/COMP={1, "value"}/VAR=1/N=wave_txt GroupID, "actuator_1_1"
											result_txt = wave_txt[0]
																							
												if (stringmatch(result_txt,"*mono1/energy")==1)
														
													var = 7
												else
												endif
										else
										endif


			else
			
			var = -1
			endif

	return var

End

//////////////////////////////////////

Function FillFileNames(pathName)   /// Devo trovare il modo di escludere i file .SPE che non sono immagini (focus)
String pathName

String CurrentDataFolder=GetDataFolder(1)

		String tmpName
		Variable val
		
		newpath/O DataPath pathName

		String list = IndexedFile(DataPath, -1, ".nxs")	// Get a semicolon-separated list of all .nxs files in the folder 
	
		list = SortList(list, ";", 16)	// Sort using combined alpha and numeric sort   --> C'EST MIEUX  UNE ORDRE PAR DATE

		// Process the list 
		Variable numItems = ItemsInList(list) 
			Make/O/T/n=(numitems) ListDataFiles
			Make/O/n=(numitems,1,2) ListDataFilesSelect
				SetDimLabel 2,2,foreColors,ListDataFilesSelect
			Make/O/n=(numitems,3) ListDataFilesColors
			Make/O/n=(numitems,5) :Cosmics:CosmicsStoreWave
			ListDataFilesSelect=0
			ListDataFilesSelect[][][%foreColors]= 0



		Variable i
		for(i=0; i<numItems; i+=1)

		ListDataFiles[i]= StringFromList(i, list)
		
		tmpName = pathName + StringFromList(i, list)
			
//			if(Which_Scan(tmpName)==1)
//			
//			endif

// Result of t:
// 1 --> RIXS
// 2 --> XAS beamlineenergy/energy scan
// 3 --> tx-scan
// 4 --> tz-scan
// 5 --> smono-posx
// 6 --> srixs_gapz
// 7 --> mono/energy scan

			switch(Which_Scan(tmpName))
			
			case 1:
				Print  StringFromList(i, list) + " is a RIXS acquisitiion" 

				break
			
			case 2:
				Print  StringFromList(i, list) + " is XAS" 
				
				break

			case 3:
				Print  StringFromList(i, list) + " is a sample-tx scan" 
				
				break

			case 4:
				Print  StringFromList(i, list) + " is a sample-tz scan" 
				
				break

			case 5:
				Print  StringFromList(i, list) + " is a smono-posx scan" 
				
				break

			case 6:
				Print  StringFromList(i, list) + " is a srixs_gapz scan" 
				
				break

			case 7:
				Print  StringFromList(i, list) + " is a mono/energy scan" 
				
				break

			case -1:
				Print  StringFromList(i, list) + " has no data !" 
				
				break
			
				
		endswitch
			
		ListBox listWaves,colorWave=ListDataFilesColors


SetDataFolder CurrentDataFolder

endfor
End
