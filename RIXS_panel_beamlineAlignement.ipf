﻿#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.



Beamline alignement Panel pour les listes : Energy / Mono H slits / RIXS V slits





function  RIXS_beamlineAlignement_DLG()


	doWindow /F RIXSbeamlineAlignementPanel
	if(V_flag == 1)
	
		return 0
	endif

	PauseUpdate; Silent 1		// building window...
	NewPanel /N=RIXSbeamlineAlignementPanel /W=(259,223,1152,715) as "Beamline alignement"
	ModifyPanel fixedSize = 1
	
	display /N=visu /HOST=RIXSbeamlineAlignementPanel /W=(22,43,582,443) 
	
	ListBox listRIXSbeamlineAlignement,pos={594.00,67.00},size={283.00,377.00}, win=RIXSbeamlineAlignementPanel, mode=4
	ListBox listRIXSbeamlineAlignement, proc=ListBoxProc_beamlineAlignement
	ListBox listRIXSbeamlineAlignement, listWave = root:RIXS:Variables:BeamlineAlignement
	ListBox listRIXSbeamlineAlignement, selWave = root:RIXS:Variables:selBeamlineAlignement
		
	PopupMenu popupBeamlineAlignement,pos={592.00,41.00},size={106.00,23.00},proc=PopMenuProc_beamlineAlignement,title="Type de scan"
	PopupMenu popupBeamlineAlignement,mode=0,value= #"\"Energy;Positions Mono H slits;Position RIXS V slits;All\""
	
	Button buttonCloseRIXSbeamlineAlignementPanel,pos={748,455},size={129,23},proc=btnProc_closeRIXSbeamlineAlignementPanel,title="Close"

	return 0
	
end





//----------------------------------------------------------------------------------------------------------------------------------
	PopMenu : liaison ListBox <==> type de liste (sélectionnée via PopMenu)
//----------------------------------------------------------------------------------------------------------------------------------
Function PopMenuProc_beamlineAlignement(ctrlName,popNum,popStr) : PopupMenuControl
	String ctrlName
	Variable popNum
	String popStr

	SVAR spectrumType = root:RIXS:Variables:spectrumType
	
	WAVE /T BeamlineAlignement = root:RIXS:Variables:BeamlineAlignement
	WAVE selBeamlineAlignement = root:RIXS:Variables:selBeamlineAlignement
	WAVE /T MonoFiles = root:RIXS:Variables:MonoFiles
	WAVE /T fent_h1Files = root:RIXS:Variables:fent_h1Files
	WAVE /T vslitFiles = root:RIXS:Variables:vslitFiles
	
	variable dim
	
	
	//  Rafraichissement général au passage d'un type de liste à l'autre
	//
	//			1/ retrait des waves du graphe
	//			2/ destruction des waves de root:RIXS:panels:beamlineAlignementFolder 
	
	string previousDF = getDataFolder(1)
	setDataFolder root:RIXS:panels:beamlineAlignementFolder
	
		string tracesList = traceNameList("RIXSbeamlineAlignementPanel#visu", ";", 1)
		variable i
		
		for (i = 0 ; i <= itemsInList(tracesList, ";") - 1 ; i += 1)
		
			RemoveFromGraph /Z/W=RIXSbeamlineAlignementPanel#visu $stringFromList(i, tracesList, ";")
		endfor
	
		KillWaves /Z/A 
	setDataFolder previousDF
	
	
	// Changement de liste
	strswitch(popStr)
	
		case "Energy" :
	
			spectrumType = "Energy"
			redimension /N=(numpnts(MonoFiles)) BeamlineAlignement, selBeamlineAlignement
			BeamlineAlignement = MonoFiles
			selBeamlineAlignement = 0
			break
			
		case "Positions Mono H slits" :
		
			spectrumType = "Positions Mono H slits"
			redimension /N=(numpnts(fent_h1Files)) BeamlineAlignement, selBeamlineAlignement
			BeamlineAlignement = fent_h1Files
			selBeamlineAlignement = 0
			break
			
		case "Position RIXS V slits" :
		
			spectrumType = "Position RIXS V slits"
			redimension /N=(numpnts(vslitFiles)) BeamlineAlignement, selBeamlineAlignement
			BeamlineAlignement = vslitFiles
			selBeamlineAlignement = 0
			break
			
		case "All" :
			
			spectrumType = "All"
			concatenate /O {MonoFiles, fent_h1Files, vslitFiles}, root:RIXS:Variables:destWave
			WAVE /T destWave = root:RIXS:Variables:destWave
			redimension /N=(numpnts(destWave)) BeamlineAlignement, selBeamlineAlignement
			BeamlineAlignement = destWave ; killWaves destWave
			selBeamlineAlignement = 0
			break
			
	endswitch
	

	return 0
End



//----------------------------------------------------------------------------------------------------------------------------------
		ListBox --> shift clic fichier.nxs
//----------------------------------------------------------------------------------------------------------------------------------
Function ListBoxProc_beamlineAlignement(lba) : ListBoxControl
	STRUCT WMListboxAction &lba

	Variable row = lba.row
	Variable col = lba.col
	WAVE/T/Z listWave = lba.listWave
	WAVE/Z selWave = lba.selWave
	
	WAVE /T MonoFiles = root:RIXS:Variables:MonoFiles			;	WAVE selMonoFiles = root:RIXS:Variables:selMonoFiles
	WAVE /T fent_h1Files = root:RIXS:Variables:fent_h1Files	;	WAVE selfent_h1Files = root:RIXS:Variables:selfent_h1Files
	WAVE /T vslitFiles = root:RIXS:Variables:vslitFiles			;	WAVE selvslitFiles = root:RIXS:Variables:selvslitFiles
	
	
	SVAR spectraType = root:RIXS:Variables:spectraType
	variable i
	string selectionList
	

	switch( lba.eventCode )
		case -1: // control being killed
			break
		case 1: // mouse down
			break
		case 3: // double click
			break
	
	
		case 4: // cell selection

			//print "lba.eventCode -->", lba.eventCode
			//RIXS_BeamlineAlignement_selectOnClic()
			 break		
		
		
		case 5: // cell selection plus shift key
		
			RIXS_BeamlineAlignement_selectOnClic()
		
			break
		case 6: // begin edit
			break
		case 7: // finish edit
			break
		case 13: // checkbox clicked (Igor 6.2 or later)
			break
	endswitch

	return 0
End





//----------------------------------------------------------------------------------------------------------------------------------
Listbox listRIXSbeamlineAlignement    mode = 4 (multisélection)

clic ==>  affichage/effacement fichier.nxs ciblé

	// faire un scan de selBeamlineAlignement pour savoir ce qui est à 1 et qui doit être affiché
	// S'aider de traceNameList pour savoir ce qui est déjà affiché (éviter d'aller charger une deuxième fois
	// un fichier.nxs déjà affiché).
	
	// aller récupérer les fichiers.nxs (selBeamlineAlignement à 1) et les charger dans root:RIXS:Variables:BeamlineAlignement
	
	//	faire un appendtograph si 1er fois ou un trace on/off selon sélection(1)/déselection(0)
	
	// Infos________________	
	// traceNameList
	// removeFromGraph /Z /W=RIXSbeamlineAlignementPanel#visu nom_Wave
	// AppendToGraph /W = RIXSbeamlineAlignementPanel#visu nom_Wave
	// _____________________

//----------------------------------------------------------------------------------------------------------------------------------
function RIXS_BeamlineAlignement_selectOnClic()

	SVAR dataPathFile = root:RIXS:Variables:dataPathFile
	
	WAVE /T BeamlineAlignement		= root:RIXS:Variables:BeamlineAlignement 
	WAVE 	selBeamlineAlignement		= root:RIXS:Variables:selBeamlineAlignement
	WAVE rgb_R = root:RIXS:Variables:rgb_R
	WAVE rgb_G = root:RIXS:Variables:rgb_G
	WAVE rgb_B = root:RIXS:Variables:rgb_B
	
	SVAR spectrumType = root:RIXS:Variables:spectrumType
	
	variable FileID, GroupID	
	string previous_DF
	variable i, j, k 
	string groupePrincipal, dataSets_list, dataSetAttr_List, listItem, cible, listeCibles, tangoAddress, listeWave
	variable taille_list 
	variable goFrom, charComp
	string data = "data_01"
	string trajectory = "trajectory_1_1"
	

	// CHARGEMENT des fichiers.nxs sélectionnés à chaque shift-clic --> Scan selBeamlineAlignement	
	//
	//		si à 1 : Chargement fichier.nxs ciblé  --> cible
	//		si à 0 : killWaves /Z nom_Wave_ciblée. Utiliser "WaveList(matchStr, separatorStr, optionsStr )"
	//listeCibles = ""
	
	previous_DF = getDataFolder(1) //; print "previous_DF -->", previous_DF
	setDataFolder root:RIXS:panels:beamlineAlignementFolder
	
	for(i = 0 ; i <= numpnts(selBeamlineAlignement) - 1 ; i += 1)
	
		if(selBeamlineAlignement[i] == 1)
		
			cible = BeamlineAlignement[i] //; print "BeamlineAlignement[",i,"] -->", cible
			
			// Chargement fichier.nxs
				HDF5OpenFile /R FileID as dataPathFile + cible			// Chargement du fichier.nxs[i]
				//print "HDF5OpenFile : V_Flag -->", V_Flag
			
				HDF5Dump /A /ATTR = 0 /Q dataPathFile + cible				// Récupération du nom du groupe principal 
				goFrom = strSearch(S_HDF5Dump, "\"scan", 0)
				j = 1
					do
					
						charComp = cmpstr(S_HDF5Dump[goFrom + j], "\"")
						j += 1
					while(charComp != 0)
				groupePrincipal = S_HDF5Dump[goFrom + 1, goFrom + j-2] //; print "groupePrincipal -->", groupePrincipal
				
				// Ouverture groupe fichier.nxs
				HDF5OpenGroup FileID, groupePrincipal + "/scan_data", GroupID				
				
				// Récupération de la DataSetsList
				HDF5ListGroup FileID, groupePrincipal + "/scan_data"					
				dataSets_list = S_HDF5ListGroup //; print "dataSets_list -->", dataSets_list		
				
				// Récupération 1er elt de la DataSetList pour l'étape d'après (récupération adresse tango)
				HDF5ListAttributes GroupID, stringFromList(0, dataSets_list, ";") //; print "1er elt de la DataSetList -->", stringFromList(0, dataSets_list, ";")
			
				// Récupération de l'adresse Tango (celle correspondant au 1er élément de la dataSetsList)
				HDF5Dump /A = groupePrincipal + "/scan_data/" + stringFromList(0, dataSets_list, ";") + "/long_name" /Q dataPathFile + cible 
				
					goFrom = strsearch(S_HDF5Dump, "i14-m", 0)
					j = 0
					do
						charComp = cmpstr(S_HDF5Dump[goFrom + j], "\"")
						j += 1  
					while(charComp != 0)
					tangoAddress = S_HDF5Dump[goFrom, goFrom + j - 2] //; print "tangoAddress -->", 	tangoAddress ; print " "	
		
				
				// Chargement data_01 et trajectory_1_1 du fichier.nxs dans root:RIXS:panels:beamlineAlignementFolder
				// Si les waves existe déjà on les écrase (option /O de HDF5LoadData)
				HDF5LoadData/O/Q/VAR=0/N=$(groupePrincipal + "_" + data) GroupID, data
				HDF5LoadData/O/Q/VAR=0/N=$(groupePrincipal + "_" + trajectory) GroupID, trajectory


				HDF5CloseGroup	GroupID
				HDF5CloseFile FileID

		
		else	// on retire du graphe et détruit, qu'elle existe ou non, la cible (data_01, trajectory_1_1) de root:RIXS:panels:BeamlineAlignementFolder
			
				cible = BeamlineAlignement[i] 
					
				// Récup du groupePrincipal 
				HDF5OpenFile /R FileID as dataPathFile + cible			// Chargement du fichier.nxs[i]
									
				HDF5Dump /A /ATTR = 0 /Q dataPathFile + cible				
				goFrom = strSearch(S_HDF5Dump, "\"scan", 0)
				j = 1
					do
						
						charComp = cmpstr(S_HDF5Dump[goFrom + j], "\"")
						j += 1
					while(charComp != 0)
					groupePrincipal = S_HDF5Dump[goFrom + 1, goFrom + j-2]
						
				HDF5CloseFile FileID
					
				removeFromGraph /Z /W=RIXSbeamlineAlignementPanel#visu $(groupePrincipal + "_" + 	data)
				killWaves /Z $(groupePrincipal + "_" + 	data)
				
				removeFromGraph /Z /W=RIXSbeamlineAlignementPanel#visu $(groupePrincipal + "_" + 	trajectory)
				killWaves /Z $(groupePrincipal + "_" + 	trajectory)
					
		endif

	endfor
	
	
	
	// Affichage data_01 = f(trajectory_1_1)
	// graph : RIXSbeamlineAlignementPanel#visu
	string tracesList = traceNameList("RIXSbeamlineAlignementPanel#visu", ";", 1)
	string wavesList_data = waveList("*data*", ";", "")
	string wavesList_trajectory = waveList("*trajectory*", ";", "")
	
	for (i = 0 ; i <= itemsInList(tracesList, ";") - 1  ; i += 1)
	
		removeFromGraph /Z /W=RIXSbeamlineAlignementPanel#visu $stringFromList(i, tracesList, ";")
	endfor
	
	
	for (i = 0 ; i <= itemsInList(wavesList_data, ";") - 1  ; i += 1)
	
		appendToGraph /W=RIXSbeamlineAlignementPanel#visu /C=(rgb_R[i], rgb_G[i], rgb_B[i]) $stringFromList(i, wavesList_data, ";")
		
	endfor

	legend /W=RIXSbeamlineAlignementPanel#visu /C /N=legende
	
	Label /Z /W=RIXSbeamlineAlignementPanel#visu left "\\Z14Diode signal (a.u.)"
		
	string abcisse
	
	strSwitch (spectrumType)
	
		case "Energy" :
				abcisse = "\\Z14Energy (eV)"
			break
			
		case "Positions Mono H slits" :
		
				abcisse = "\\Z14Positions Mono H slits (mm)"
			break
			
		case "Position RIXS V slits" :
		
				abcisse = "\\Z14Position RIXS V slits (mm)"
			break
			
		case "All" :
		
				abcisse = "\\Z14All"
			break
			
		case "nothing" :
		
				abcisse = "\\Z14All"
			break
	endswitch

	Label /Z /W=RIXSbeamlineAlignementPanel#visu bottom abcisse
	
	
		
	setDataFolder previous_DF	
	
	return 0
end





//----------------------------------------------------------------------------------------------------------------------------------
	Close beamlineAlignement  Panel
//----------------------------------------------------------------------------------------------------------------------------------
Function btnProc_closeRIXSbeamlineAlignementPanel(ctrlName) : ButtonControl
	String ctrlName
	
	killWindow RIXSbeamlineAlignementPanel
	
	return 0
End










