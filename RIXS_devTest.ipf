#pragma TextEncoding = "MacRoman"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.


menu "tests"

	"test WaveList", test_WaveList()
end



function test_WaveList()

	string prefix = "toto"
	
	Make /O/N=20 totoWave1 = gnoise(1)
	Make /O/N=20 totoWave2 = gnoise(2)
	Make /O/N=20 totoWave3 = gnoise(3)
	Make /O/N=20 titiWave10 = gnoise(10)
	Make /O/N=20 titiWave20 = gnoise(20)
	Make /O/N=20 titiWave30 = gnoise(30)

	string liste = waveList(prefix + "*", ";", "")
	print "liste -->", liste

	return 0
end

_______________________________________________________________________________________________________________________
menu "tests"

	"test rename", test_rename()
end

function test_rename()

	Make /O/N=20 wave01= gnoise(1)
	string newWave = "prefix_wave01"
	
	rename wave01, newWave	// ici wave01 est chang� par newWave. Si $newWave utilis� --> wave01 est chang� par prefix_wave01 
	
	// rename wave01, "prefix_wave01"  -->  Ne compile pas

	return 0
end

 
_______________________________________________________________________________________________________________________
menu "tests"

	"test buildGraph et remove", test_buildGraph()
end

function test_buildGraph()

	Make /O/N=100 wave1 = gnoise(1)
	Make /O/N=100 wave2 = gnoise(1) + p
	Make /O/N=100 wave3 = p
	
	Display /N=showGraph /K=1 /W=(532,126,1203,507)
	
	AppendToGraph /W=showGraph wave1 wave2 vs wave3
	
	return 0
end

function test_removeGraph()

	WAVE wave2 = root:wave2
	
	RemoveFromGraph /Z /W=showGraph wave2

	return 0
end
_______________________________________________________________________________________________________________________




concatenate




_______________________________________________________________________________________________________________________
menu "tests"

	"test plotRightLeft", test_plotRightLeft()
end

function test_plotRightLeft()

	Make /O/N=100 RightPlotWave = gnoise(1)
	Make /O/N=100 LeftPlotWave = gnoise(1) + p
	Make /O/N=100 abcisse = p
	
	Display /N=plotRightLeft
	
	AppendToGraph /W=plotRightLeft /R /C =(0,0,65280) RightPlotWave vs abcisse
	ModifyGraph /W=plotRightLeft axRGB(right)=(0,0,65280)
	Label right "Plot to the right"

	AppendToGraph /W=plotRightLeft /L /C =(65280,0,0) LeftPlotWave vs abcisse
	ModifyGraph /W=plotRightLeft axRGB(right)=(65280,0,0)
	Label left "Plot to the left"

	return 0
end



_______________________________________________________________________________________________________________________
menu "tests"

	"-"
	"test traceNameList", test_traceNameList()
	"test remove from tracesOnGraph", test_removeFrom_tracesOnGraph()
	"-"
end

function test_traceNameList()

	Make /O/N=100 Wave01 = gnoise(1)  
	Make /O/N=100 Wave02 = gnoise(1) + p
	Make /O/N=100 Wave03 = p
	Make /O/N=100 Wave04 = enoise(12)
	
	Display /N=tracesOnGraph
	
	AppendToGraph /W=tracesOnGraph Wave01, Wave02, Wave03, Wave04
	
	string tracesList = traceNameList("tracesOnGraph", ";", 1) ; print "tracesList -->", tracesList	

	return 0
end

function test_removeFrom_tracesOnGraph()

	WAVE wave01 = root:wave01
	WAVE wave02 = root:wave02
	WAVE wave03 = root:wave03
	WAVE wave04 = root:wave04
	
	string tracesListe = "Wave01;Wave02;Wave03;Wave04;"
	variable i
	
	for (i = 0 ; i <= itemsInList(tracesListe, ";") - 1 ; i += 1)
	
		RemoveFromGraph /W=tracesOnGraph $stringFromList(i, tracesListe, ";")
	endfor
	
	return 0
end



_______________________________________________________________________________________________________________________
menu "tests"

	"-"
	"wave RGB", waveRGB()
	"-"
end


function waveRGB()

	Make /O/N=100 noiseColor = gnoise(1)
	
	Display /N=showColor noiseColor
	modifyGraph /W=showColor rgb=(0,65535,65535)	
	
		
	//modifyGraph /W=showColor rgb=(0,0,65535)				// bleu
	// rgb(0,0,0)					// noir
		// rgb(65535,65535,65535)	// blanc - inutilisable
	
	// rgb(0,0,65535)				// Bleu
	// rgb(0,65535,0)				// Vert
	// rgb(65535,0,0)				// Rouge
	
		// rgb(65535,65535,0)			// Jaune - inutilisable
	// rgb(65535,0,65535)			// Fushia
	// rgb(0,65535,65535)			// Cyan

	
	// rgb(52428,17472,1)			// marron
	// rgb(52428,34958,1)			// marron clair
	// rgb(65535,49157,16385)	// Orange
	// rgb(65535,49151,62258)	// Rose
	// rgb(49151,49152,65535)	// mauve	

	return 0
end

function paletteCouleurs()

	Make /O/N=11 rgb_R = {0, 0, 0, 65535, 65535, 0, 52428, 52428, 65535, 65535, 49151}
	Make /O/N=11 rgb_G = {0, 0, 65535, 0, 0, 65535, 17472, 34958, 49157, 49151, 49152}
	Make /O/N=11 rgb_B = {0, 65535, 0, 0, 65535, 65535, 1, 1, 16385, 62258, 65535}
	
	string rgbList = "rgb_R;rgb_G;rgb_B"
	
	save /O/G/W/B rgbList as "C:prgms:Igor:Alessandro_RIXS:prjRIXS:palette.txt"
	
	return 0
end


function loadPalette()

	LoadWave /A/W/O/Q/J/K=1  "C:prgms:Igor:Alessandro_RIXS:prjRIXS:palette.txt"

	return 0
end




_______________________________________________________________________________________________________________________
menu "tests"

	"-"
	"Normalisation wave", normalisation()
	"-"
end

// waveStats
// norm
// MatrixOp

function normalisation()

	Make /O/N=100 gaussienne = gauss(x,50,10)
	
	print " "
	print "norm(gaussienne) -->", norm(gaussienne) ; print " "
	
	waveStats /Q gaussienne ; print "waveStats sur gaussienne"
	print "V_npnts -->", V_npnts
	print "V_avg -->", V_avg
	print "V_sum -->", V_sum
	print "V_sdev -->", V_sdev
	print "V_minloc -->", V_minloc
	print "V_min -->", V_min
	print "V_maxloc -->", V_maxloc
	print "V_max -->", V_max
	print " "
	print "______________________________________"
	
	duplicate /O gaussienne gaussienneNormalisee
	
	WAVE gaussienne = root:gaussienne
	WAVE gaussienneNormalisee = root:gaussienneNormalisee
	
	gaussienneNormalisee = gaussienne/V_max
	
	Display /N=showGauss /W=(35,42,430,250) /K=0 gaussienne
	Display /N=showGaussNorm /W=(437,42,832,250) /K=0 gaussienneNormalisee

	return 0
end




_______________________________________________________________________________________________________________________
menu "tests"

	"-"
	"Gaussienne", GaussGraph()
	"Curseur", Curseur()
	"-"
end

csrWave CsrWaveRef
CsrXWave	CsrXWaveRef
ShowInfo HideInfo 
Cursor
CsrInfo

function GaussGraph()

	Make /O/N=100 gaussienne = gauss(x,50,10)
	Display /N=showGauss /W=(35,42,430,250) /K=0 gaussienne
	
	ShowInfo /CP=0 /W=showGauss
	
	return 0
end


function Curseur()
	
	Print "vcsr : A", vcsr(A, "showGauss")
	Print "pcsr : A", pcsr(A, "showGauss")
	Print "qcsr : A", qcsr(A, "showGauss")
	Print "hcsr : A", hcsr(A, "showGauss")
	Print "xcsr : A", xcsr(A, "showGauss")
	Print "zcsr : A", zcsr(A, "showGauss")

	return 0
end





_______________________________________________________________________________________________________________________
menu "tests"


	"Vertical", Vertical()

	"-"
end



function Vertical()

	
	Make /O/N=50 waveNoise = gnoise(1) + p
	Make /O/N=150 gaussienne = 2*10^3*gauss(x,50,10)
	
//	Make /O/N=2 ligneVerticale
//	ligneVerticale[0] = 20
//	ligneVerticale[1] = 30
	
	Make /O/N=150 abcisse = p //*0.25
	Make /O/N=150 abcisseBruit = p*0.25 + gnoise(0.05)
	

	
	Display /N=showWave /W=(549,186,1287,618)
	
	ControlBar /W=showWave 90
	
	Slider sliderVertical,pos={40.00,39.00},size={677.00,50.00}
	Slider sliderVertical,limits={0,2,1},value= 0,vert= 0
	CheckBox checkShowCursors,pos={43.00,11.00},size={82.00,16.00},proc=CheckProc_testVerticalShowCursors,title="Show cursors"
	CheckBox checkShowCursors,value= 0
	SetVariable setvarVerticalPosition,pos={273.00,12.00},size={150.00,19.00},title="Position"
	SetVariable setvarVerticalPosition,limits={-inf,inf,0}
	
	appendToGraph /W=showWave gaussienne vs abcisseBruit
	appendToGraph /W=showWave waveNoise vs abcisse
	ShowInfo /CP=0 /W=showWave
	//HideInfo /W=showWave
	
		

	return 0
end



Function CheckProc_testVerticalShowCursors(ctrlName,checked) : CheckBoxControl
	String ctrlName
	Variable checked

End


_______________________________________________________________________________________________________________________
TileWindows
StackWindows
setscale






















