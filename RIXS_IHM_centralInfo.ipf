#pragma TextEncoding = "MacRoman"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.




function RIXScentralInfoWindow()

	doWindow /F RIXScentralPanel
	if(V_flag == 1)
	
		return 0
	endif
	
	
	PauseUpdate; Silent 1		// building window...
	NewPanel /N=RIXScentralPanel /W=(208,70,1507,718) as "Files infos panel"
	ModifyPanel fixedSize=1
	
	SetDrawLayer UserBack
	SetDrawEnv /W=RIXScentralPanel fsize= 14
	DrawText /W=RIXScentralPanel 103,26,"____________________  nxs Files  ____________________"
	DrawText /W=RIXScentralPanel 85,58,"RIXS"
	DrawText /W=RIXScentralPanel 281,58,"XAS"
	DrawText /W=RIXScentralPanel 466,58,"energy"
	DrawText /W=RIXScentralPanel 73,361,"Sample-tx"
	DrawText /W=RIXScentralPanel 262,361,"Sample-tz"
	SetDrawEnv /W=RIXScentralPanel fsize= 14
	DrawText /W=RIXScentralPanel 1016,58,"Contextual data"
	DrawText /W=RIXScentralPanel 454,361,"fent_h1"
	DrawText /W=RIXScentralPanel 644,361,"vslit"
	
	display /N=visuel /HOST=RIXScentralPanel /W=(817,347,1276,628)
	
	
	// cdc Alessandro : chargement uniquement via l'IHM RIXS_IHM_edge.ipf	
	// Chemin fichier.nxs
//	SetVariable setvarDataPathSpectra,pos={25,12},size={558,15},proc=SetVarProc_onEnterDataPath,title="Path : "
//	SetVariable setvarDataPathSpectra,limits={-inf,inf,0},value= root:RIXS:Variables:dataPathFile
	
	
	// ListBox RIXS
	ListBox listRIXSscans,pos={18,65},size={181,263},proc=ListBoxProc_selectFile
	ListBox listRIXSscans,listWave=root:RIXS:Variables:RIXSfiles
	ListBox listRIXSscans,selWave=root:RIXS:Variables:selRIXSfiles,mode= 2,selRow= 0
	//ListBox listRIXSscans,selRow= 0
	
	
	// ListBox XAS
	ListBox listXASscans,pos={207,64},size={181,263},proc=ListBoxProc_selectFile
	ListBox listXASscans,listWave=root:RIXS:Variables:XASfiles
	ListBox listXASscans,selWave=root:RIXS:Variables:selXASfiles,mode= 2,selRow= 0
	//ListBox listXASscans,selRow= 0
	
	
	// ListeBox Mono (energy)
	ListBox listMonoScans,pos={396,64},size={181,263},proc=ListBoxProc_selectFile
	ListBox listMonoScans,listWave=root:RIXS:Variables:MonoFiles
	ListBox listMonoScans,selWave=root:RIXS:Variables:selMonoFiles,mode= 2,selRow= 0
	//ListBox listMonoScans,selRow= 0
	
	
	// ListBox Sample-tx
	ListBox listSampleTxScans,pos={18,367},size={181,263},proc=ListBoxProc_selectFile
	ListBox listSampleTxScans,listWave=root:RIXS:Variables:Sample_txFiles
	ListBox listSampleTxScans,selWave=root:RIXS:Variables:selSample_txFiles
	ListBox listSampleTxScans,mode= 2 //,selRow= 0
	
	
	// ListBox Sample-tz
	ListBox listSampleTzScans,pos={207,367},size={181,263},proc=ListBoxProc_selectFile
	ListBox listSampleTzScans,listWave=root:RIXS:Variables:Sample_tzFiles
	ListBox listSampleTzScans,selWave=root:RIXS:Variables:selSample_tzFiles,mode= 2
	ListBox listSampleTzScans,mode= 2  //,selRow= 0


	// ListBox fent_h1
	ListBox listFent_h1,pos={397,367},size={181,263},proc=ListBoxProc_selectFile
	ListBox listFent_h1,listWave=root:RIXS:Variables:fent_h1Files
	ListBox listFent_h1,selWave=root:RIXS:Variables:selfent_h1Files
	ListBox listFent_h1,mode= 2  //,selRow= 0
	
	// ListBox vslit
	ListBox listVslit,pos={587,367},size={181,263},proc=ListBoxProc_selectFile
	ListBox listVslit,listWave=root:RIXS:Variables:vslitFiles
	ListBox listVslit,selWave=root:RIXS:Variables:selvslitFiles,mode= 2
	ListBox listVslit,mode= 2  //,selRow= 0
	
	
	// ListBox Meta Data
	ListBox listMetaData,pos={817,65},size={463,261}
	ListBox listMetaData,listWave=root:RIXS:Variables:scanMetaDataWave
	ListBox listMetaData,selWave=root:RIXS:Variables:selScanMetaDataWave,mode= 2
	//ListBox listMetaData,selRow= 0
	
	
//	Display/W=(817,347,1276,628)/HOST=# 
//	RenameWindow #,visuel
//	SetActiveSubwindow ##
	
	Button closeInfoMetaData,pos={1151.00,17.00},size={129,23},proc=BtnProc_closeRIXScentralPanel,title="Fermer"

	return 0
end



Function ListBoxProc_selectFile(lba) : ListBoxControl
	STRUCT WMListboxAction &lba

	Variable row = lba.row
	Variable col = lba.col
	WAVE/T/Z listWave = lba.listWave
	WAVE/Z selWave = lba.selWave
	
	SVAR nxsSelected = root:RIXS:Variables:nxsSelected

	switch( lba.eventCode )
		case -1: // control being killed
			break
		case 1: // mouse down
			break
		case 3: // double click
			break
		
		case 4: // cell selection

				if(row >= dimSize(listWave, 0))	// Si on clic sur un espace blanc de la listBox --> Evite retour d�bogueur
					Break
				endif
				
				nxsSelected = listWave[row] ; print "nxsSelected --> ", nxsSelected
				RIXS_loadNXS()
			break
		
		case 5: // cell selection plus shift key
			break
		case 6: // begin edit
			break
		case 7: // finish edit
			break
		case 13: // checkbox clicked (Igor 6.2 or later)
			break
	endswitch

	return 0
End




Function ListBoxProc_____selectFile(ctrlName,row,col,event) : ListBoxControl
	String ctrlName
	Variable row
	Variable col
	Variable event	//1=mouse down, 2=up, 3=dbl click, 4=cell select with mouse or keys
							 //5=cell select with shift key, 6=begin edit, 7=end
					
	WAVE /T RIXSfiles = root:RIXS:Variables:RIXSfiles
	WAVE /T XASfiles = root:RIXS:Variables:XASfiles
	WAVE /T MonoFiles = root:RIXS:Variables:MonoFiles
	WAVE /T Sample_txFiles = root:RIXS:Variables:Sample_txFiles
	WAVE /T Sample_tzFiles = root:RIXS:Variables:Sample_tzFiles
	WAVE /T fent_h1Files = root:RIXS:Variables:fent_h1Files
	WAVE /T vslitFiles = root:RIXS:Variables:vslitFiles
	
	SVAR nxsSelected = root:RIXS:Variables:nxsSelected //; nxsSelected = ""
	
	if (event == 4)
	
		strSwitch(ctrlName)
		
			case "listRIXSscans" :
			 
				nxsSelected = RIXSfiles[row]
				break
				
				
			case "listXASscans" :
			
				nxsSelected = XASfiles[row]
				break
				
				
			case "listMonoScans" :
				
				nxsSelected = MonoFiles[row]
				break
				
				
			case "listSampleTxScans" :
				
				nxsSelected = Sample_txFiles[row]
				break
				
				
			case "listSampleTzScans" :
				
				nxsSelected = Sample_tzFiles[row]
				break
				
				
			case "listFent_h1" :
				
				nxsSelected = fent_h1Files[row]
				break
				
			case "listVslit" :
				
				nxsSelected = vslitFiles[row]
				break
		
		endSwitch
	endif
	
	RIXS_loadNXS()
	
	
//	if (cmpstr(nxsSelected, "") == 0)
//	
//		doAlert /T = "Archtung s�lection !" 0, "Le fichier ne r�pond pas � la s�lection --> voir la function ListBoxProc_selectFile dans RIXS_centralInfo.ipf"  
//	else
//		
//		loadNXS()
//	endif

	return 0
End



Function BtnProc_closeRIXScentralPanel(ctrlName) : ButtonControl
	String ctrlName

	killWindow RIXScentralPanel
	return 0
End




________________________________________________________________________________________________________

//	doWindow /F RIXScentralPanel
//	if(V_flag == 1)
//	
//		return 0
//	endif
//
//	PauseUpdate; Silent 1		// building window...
//	NewPanel /N=RIXScentralPanel /W=(121,126,1102,772) as "Files infos panel"
//	ModifyPanel fixedSize=1
//		
//	SetDrawLayer UserBack
//	DrawText /W=RIXScentralPanel 85,58,"RIXS"
//	DrawText /W=RIXScentralPanel 281,58,"XAS"
//	DrawText /W=RIXScentralPanel 466,58,"energy"
//	DrawText /W=RIXScentralPanel 73,361,"Sample-tx"
//	DrawText /W=RIXScentralPanel 262,361,"Sample-tz"
//	DrawText /W=RIXScentralPanel 733,58,"Meta data"
//	
//	display /N=visuel /HOST=RIXScentralPanel /W=(510,347,958,628)
//	
//		
//	// Chemin fichier.nxs
//	SetVariable setvarDataPathSpectra,pos={25,12},size={558,15},proc=SetVarProc_onEnterDataPath,title="Path : "
//	SetVariable setvarDataPathSpectra,limits={-inf,inf,0},value= root:RIXS:Variables:dataPathFile
//	
//	// ListBox RIXS
//	ListBox listRIXSscans,pos={18,65},size={181,263},proc=ListBoxProc_selectFile			//,proc=ListBoxProc_selectFile
//	ListBox listRIXSscans,listWave=root:RIXS:Variables:RIXSfiles
//	ListBox listRIXSscans,selWave=root:RIXS:Variables:selRIXSfiles,mode= 2
//	//ListBox listRIXSscans,selRow= 0
//	
//	// ListBox XAS
//	ListBox listXASscans,pos={207,64},size={181,263},proc=ListBoxProc_selectFile
//	ListBox listXASscans,listWave=root:RIXS:Variables:XASfiles
//	ListBox listXASscans,selWave=root:RIXS:Variables:selXASfiles,mode= 2
//	//ListBox listXASscans,selRow= 0
//	
//	// ListeBox Mono (energy)
//	ListBox listMonoScans,pos={396,64},size={181,263},proc=ListBoxProc_selectFile
//	ListBox listMonoScans,listWave=root:RIXS:Variables:MonoFiles
//	ListBox listMonoScans,selWave=root:RIXS:Variables:selMonoFiles,mode= 2
//	//ListBox listMonoScans,selRow= 0	
//	
//	// ListBox Sample-tx
//	ListBox listSampleTxScans,pos={18,367},size={181,263},proc=ListBoxProc_selectFile
//	ListBox listSampleTxScans,listWave=root:RIXS:Variables:Sample_txFiles
//	ListBox listSampleTxScans,selWave=root:RIXS:Variables:selSample_txFiles
//	ListBox listSampleTxScans,mode= 2 //,selRow= 0
//	
//	// ListBox Sample-tz
//	ListBox listSampleTzScans,pos={207,367},size={181,263},proc=ListBoxProc_selectFile
//	ListBox listSampleTzScans,listWave=root:RIXS:Variables:Sample_tzFiles
//	ListBox listSampleTzScans,selWave=root:RIXS:Variables:selSample_tzFiles
//	ListBox listSampleTzScans,mode= 2 //,selRow= 0
//	
//	// ListBox Meta Data
//	ListBox listMetaData,pos={587,65},size={376,261}
//	ListBox listMetaData,listWave=root:RIXS:Variables:scanMetaDataWave
//	ListBox listMetaData,selWave=root:RIXS:Variables:selScanMetaDataWave,mode= 2
//	//ListBox listMetaData,selRow= 0
//	
//	Button closeInfoMetaData,pos={850,14},size={112,32},proc=BtnProc_closeRIXScentralPanel,title="Fermer"








	