#pragma TextEncoding = "MacRoman"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.





_______________________________________________________________________________



										RIXS  /  Fonctions utilitaires g�n�rales


_______________________________________________________________________________



function RIXS_resetLists()

	string previous_df = getdatafolder(1)
	
		setDataFolder root:RIXS:Variables

			// Tous les fichiers nxs "viables" charg�s
			WAVE /T nxsScansFiles ; redimension /N = 0 nxsScansFiles
			WAVE selNxsScansFiles ; redimension /N = 0 selNxsScansFiles
			
			// Table de correspondance ficchier.nxs <--> type d'exp�rience 
			WAVE /T nxsScansFile_fileType ; redimension /N = 0 nxsScansFile_fileType
			
			// Les donn�es contextuelles d'un fichier.nxs donn�
			WAVE /T scanMetaDataWave ; redimension /N = 0 scanMetaDataWave
			WAVE selScanMetaDataWave ; redimension /N = 0 selScanMetaDataWave
			
			// Listes relatives � chaque type d'exp�rience
			WAVE /T RIXSfiles ; redimension /N = 0 RIXSfiles
			WAVE selRIXSfiles ; redimension /N = 0 selRIXSfiles
			WAVE /T XASfiles ; redimension /N = 0 XASfiles
			WAVE selXASfiles ; redimension /N = 0 selXASfiles
			WAVE /T MonoFiles ; redimension /N = 0 MonoFiles
			WAVE selMonoFiles ; redimension /N = 0 selMonoFiles
			WAVE /T Sample_txFiles ; redimension /N = 0 Sample_txFiles
			WAVE selSample_txFiles ; redimension /N = 0 selSample_txFiles
			WAVE /T Sample_tzFiles ; redimension /N = 0 Sample_tzFiles
			WAVE selSample_tzFiles ; redimension /N = 0 selSample_tzFiles
			WAVE /T fent_h1Files ; redimension /N = 0 fent_h1Files
			WAVE selfent_h1Files ; redimension /N = 0 selfent_h1Files
			WAVE /T vslitFiles ; redimension /N = 0 vslitFiles
			WAVE selvslitFiles ; redimension /N = 0 selvslitFiles
			
		setDataFolder previous_df

	return 0
end



______________________________________________________________________
onEnterDataPath : pour charger la liste des fichiers.nxs contenus dans Datapath

1 / R�cup�ration de la liste "list" des fichiers.nxs dans un r�pertoire choisi par l'utiliateur
2 / On ordonne la "list" selon ls noms des fichiers.nxs
3 / On �limine les fichiers.nxs qui ne veulent pas s'ouvrir et ceux ne comportant pas les groupes "SEXTANTS" ou "scan_data"
4 / conversion "list" lav�e des ses fichiers.nxs qui posent pb (3 /) vers la wave nxsScansFiles (pour ListBox listRIXSspectraScans)
5 / Appel de RIXS_trieParType() pour r�partir les fichiers.nxs selon leurs tyes (energy, iamge RIXS, sample-tx, etc...) --> Pour r�partition sur les diff�rentes listBox
______________________________________________________________________

function RIXS_chargementNXS()

	RIXS_resetLists()

	SVAR dataPathFile = root:RIXS:Variables:dataPathFile
	
	WAVE /T nxsScansFiles = root:RIXS:Variables:nxsScansFiles
	WAVE selNxsScansFiles = root:RIXS:Variables:selNxsScansFiles
	SVAR dataPathFile  = root:RIXS:Variables:dataPathFile
	
	SVAR unOpenFilesList = root:RIXS:infos_Problemes:unOpenFilesList ; unOpenFilesList = ""
	SVAR missGroupFilesList = root:RIXS:infos_Problemes:missGroupFilesList ; missGroupFilesList = ""
	SVAR groupeEssentiel = root:RIXS:infos_Problemes:groupeEssentiel

	// V�rif du ":" final
	variable nbChar = strlen(dataPathFile)
	if (cmpstr(dataPathFile[nbChar - 1], ":") != 0)
	
		dataPathFile = dataPathFile + ":"
	endif 
	
	newpath/O DataPath dataPathFile
	
	// R�cup�ration des fichier.nxs file dans dataPathFile
	String list = IndexedFile(DataPath, -1, ".nxs")
	
	// Sortie ordonn�e
	list = SortList(list, ";", 16)
	
	
	// V�rification si les fichiers s'ouvrent bien. Cas contraire --> pas pris en compte
	// + v�rification que les groupes cl�s (scan_xyz, SEXSTANTS, scan_data) sont bien pr�sents
	// Les fichiers qui posent probl�me sont enlev�s de la liste
	variable nbEltsInList = itemsInList(list, ";")
	variable i, FileID
	string nxsMainGroup


		
	for (i = 0 ; i < nbEltsInList ; i += 1) 

		// Est-ce que le fichier s'ouvre ?
		HDF5OpenFile /Z /R FileID as dataPathFile + StringFromList(i, list, ";")	
		
			if (V_Flag != 0)	// Ne s'ouvre pas
				
				unOpenFilesList += StringFromList(i, list, ";") + ";"					// liste des fichiers qui ne veulent pas s'ouvrir
//				list = removeFromList(StringFromList(i, list, ";"), list, ";")		// on ampute list du fichier qui ne s'ouvre pas
				HDF5CloseFile /Z FileID
				continue
			endif
		
		
		
			// Est-ce que le groupe "scan_xyz" est pr�sent ?	( <=> scan_xyz(.nxs) au bug d'ind�xation pr�s sur le xyz)
			HDF5ListGroup /TYPE=1 FileID, "/" ; nxsMainGroup = StringFromList(0, S_HDF5ListGroup, ";")
						
				if(cmpstr(nxsMainGroup, "") == 0)		// scan_xyz n'est pas l�
					
					missGroupFilesList += StringFromList(i, list, ";") + ";"
//					list = removeFromList(StringFromList(i, list, ";"), list, ";")
										
					continue			
				endif
				

				// Est-ce que les groupes "SEXSTANTS" et "scan_data" sont pr�sents ?
				HDF5ListGroup /TYPE=1 FileID, nxsMainGroup	
	
					if(WhichListItem("SEXTANTS", S_HDF5ListGroup, ";") == -1)
						
						missGroupFilesList += StringFromList(i, list, ";") + ";"
//						list = removeFromList(StringFromList(i, list, ";"), list, ";")
						HDF5CloseFile FileID
						continue
					endif
					
					
					if(WhichListItem("scan_data", S_HDF5ListGroup, ";") == -1)
						
						missGroupFilesList += StringFromList(i, list, ";") + ";"
//						list = removeFromList(StringFromList(i, list, ";"), list, ";")
						HDF5CloseFile FileID
						
						continue
					endif				

		HDF5CloseFile FileID	
	endfor
	
	
	// Mise � jour de list : TRAITEMENT de la list via un remove des elts des listes unOpenFilesList et missGroupFilesList (fichiers qui ne vont pas)
		
	if (cmpstr(unOpenFilesList, "") != 0)
	
		list = removeFromList(unOpenFilesList, list, ";")
	endif
	

	if (cmpstr(missGroupFilesList, "") != 0)
	
		list = removeFromList(missGroupFilesList, list, ";")
	endif
	
	
	nbEltsInList = itemsInList(list, ";")		// re-calcul de nbEltsInList au cas o� il y aurait eu des "remove" dans la liste  "list"
	redimension /N=(nbEltsInList) nxsScansFiles
	redimension /N=(nbEltsInList) selNxsScansFiles


	// Remplissage de la "wave /T" des fichier.nxs VALIDES dans le r�pertoire dataPathFile
	for (i = 0 ; i < nbEltsInList ; i += 1) 
	 
		nxsScansFiles[i] = stringFromList(i, list, ";")	
	endfor
	
	
	// Rapport probl�mes (fichiers inouvrable ou groupes manquant)
	if (cmpstr(unOpenFilesList, "") != 0 || cmpstr(missGroupFilesList, "") != 0)
		
		beep
		doAlert /T = "Alert Info" 0, "Probl�mes d'ouverture fichiers et/ou absence de groupes. \n\n Voir : \n\n		root:RIXS:infos_Problemes:unOpenFilesList \n		root:RIXS:infos_Problemes:missGroupFilesList"
	endif
	

	 // Trie des fichier.nxs selon leurs types (energy, image, sample-tx, sample-tz)
	RIXS_trieParType()
	
	// Correspondance fichier.nxs <=> type de fichier (RIXS, energy (mono), beamlineEnergy, sample-tx,...)
	RIXS_nxsFilesTypes()

	return 0
end





______________________________________________________________________

1 / Trie selon type de scan
2 / Chaque liste --> wave_type associ�e
3 / wave_type --> listBox associ�e

Types : 
			RIXS (image)			i14-m-cx1/dt/princeton.rixs.1/image			
			XAS (energy)			i14-m-c00/ex/beamlineenergy/energy
			Mono (energy)		i14-m-c04/op/mono1/energy
			sample-tx				i14-m-cx1/ex/sample-tx/position
			sample-tx				i14-m-cx1/ex/sample-tz/position
			fent_h1
			vslit
			
			
______________________________________________________________________

function RIXS_trieParType()
	
	// Waves ListBox
	WAVE /T nxsScansFiles = root:RIXS:Variables:nxsScansFiles
	
	WAVE /T RIXSfiles = root:RIXS:Variables:RIXSfiles							; redimension /N = 0 RIXSfiles
	WAVE /T XASfiles = root:RIXS:Variables:XASfiles								; redimension /N = 0 XASfiles
	WAVE /T MonoFiles = root:RIXS:Variables:MonoFiles							; redimension /N = 0 MonoFiles
	WAVE /T Sample_txFiles = root:RIXS:Variables:Sample_txFiles				; redimension /N = 0 Sample_txFiles
	WAVE /T Sample_tzFiles = root:RIXS:Variables:Sample_tzFiles				; redimension /N = 0 Sample_tzFiles
	WAVE /T fent_h1Files = root:RIXS:Variables:fent_h1Files					; redimension /N = 0 fent_h1Files
	WAVE /T vslitFiles = root:RIXS:Variables:vslitFiles							; redimension /N = 0 vslitFiles
	
	WAVE selRIXSfiles = root:RIXS:Variables:selRIXSfiles						; redimension /N = 0 selRIXSfiles
	WAVE selXASfiles = root:RIXS:Variables:selXASfiles							; redimension /N = 0 selXASfiles
	WAVE selMonoFiles = root:RIXS:Variables:selMonoFiles						; redimension /N = 0 selMonoFiles
	WAVE selSample_txFiles = root:RIXS:Variables:selSample_txFiles			; redimension /N = 0 selSample_txFiles
	WAVE selSample_tzFiles = root:RIXS:Variables:selSample_tzFiles			; redimension /N = 0 selSample_tzFiles
	WAVE selfent_h1Files = root:RIXS:Variables:selfent_h1Files				; redimension /N = 0 selfent_h1Files
	WAVE selvslitFiles = root:RIXS:Variables:selvslitFiles						; redimension /N = 0 selvslitFiles
	
	//; redimension /N = 0 RIXSfiles  ?????????????????
	
	
	
	variable N1 = 0, N2 = 0, N3 = 0, N4 = 0, N5 = 0, N6 = 0, N7 = 0
	
	SVAR dataPathFile  = root:RIXS:Variables:dataPathFile
	SVAR nxsSelected = root:RIXS:Variables:nxsSelected
	SVAR nxsGroup = root:RIXS:Variables:nxsGroup
	Variable FileID 	//NVAR FileID = root:RIXS:Variables:FileID
	Variable GroupID //NVAR GroupID = root:RIXS:Variables:GroupID
	SVAR racineList = root:RIXS:Variables:racineList
	SVAR dataSetsList = root:RIXS:Variables:dataSetsList
	SVAR dataSetAttrList = root:RIXS:Variables:dataSetAttrList
	SVAR targetType = root:RIXS:Variables:targetType
	
	variable nbElts_nxsScansFiles = numPnts(nxsScansFiles)
	variable charComp, goFrom, i, j, k
		
	// D�roulement de la liste de tous les fichiers.nxs charg�s
	for (i = 0; i <= nbElts_nxsScansFiles - 1 ; i += 1)
		
			HDF5OpenFile /R FileID as dataPathFile + nxsScansFiles[i] 
			
			HDF5Dump /A /ATTR = 0 /Q dataPathFile + nxsScansFiles[i] 
			goFrom = strSearch(S_HDF5Dump, "\"scan", 0)
			j = 1
			do
		
				charComp = cmpstr(S_HDF5Dump[goFrom + j], "\"")
				j += 1
			while(charComp != 0)
			nxsGroup = S_HDF5Dump[goFrom + 1, goFrom + j-2] 

			
			// Groupe scan_data
			HDF5OpenGroup FileID, nxsGroup + "/scan_data", GroupID
			
			
			// Liste Datasets
			HDF5ListGroup FileID, nxsGroup + "/scan_data" 
			dataSetsList = S_HDF5ListGroup 
		
		
//			// Liste Dataset Attributes
//			HDF5ListAttributes GroupID, stringFromList(0, dataSetsList, ";") 
//			dataSetAttrList = S_HDF5ListAttributes 		


//			// targetType --> princeton;beamlineenergy;mono1;sample-tx;sample-tz;fent_h1;vslit;
			HDF5Dump /A = nxsGroup + "/scan_data/" + stringFromList(0, dataSetsList, ";") + "/long_name" /Q dataPathFile + nxsScansFiles[i]

			k = 0
			for (k = 0 ; k <= 5 ; k += 1)
			
				if (strsearch(S_HDF5Dump, stringFromList(k, targetType, ";"), 0) != -1)

					break
				endif
			endfor
			
			
			// Attribution selon type
			strSwitch(stringFromList(k, targetType, ";"))
			
			case "princeton" :
				N1 += 1
				Redimension /N = (N1) RIXSfiles ; Redimension /N = (N1) selRIXSfiles
				RIXSfiles[N1 - 1] = nxsScansFiles[i]
				break
				
			case "beamlineenergy" :
				N2 += 1
				Redimension /N = (N2) XASfiles ; Redimension /N = (N2) selXASfiles
				XASfiles[N2 - 1] = nxsScansFiles[i]
				break
				
			case "mono1" :
				N3 += 1
				Redimension /N = (N3) MonoFiles ; Redimension /N = (N3) selMonoFiles
				MonoFiles[N3 - 1] = nxsScansFiles[i]
				break
				
			case "sample-tx" :
				N4 += 1
				Redimension /N = (N4) Sample_txFiles ; Redimension /N = (N4) selSample_txFiles
				Sample_txFiles[N4 - 1] = nxsScansFiles[i]
				break	
				
			case "sample-tz" :
				N5 += 1
				Redimension /N = (N5) Sample_tzFiles ; Redimension /N = (N5) selSample_tzFiles
				Sample_tzFiles[N5 - 1] = nxsScansFiles[i]
				break
				
			case "fent_h1" :
				N6 += 1
				Redimension /N = (N6) fent_h1Files ; Redimension /N = (N6) selfent_h1Files
				fent_h1Files[N6 - 1] = nxsScansFiles[i]
				break
				
			case "vslit" :
				N7 += 1
				Redimension /N = (N7) vslitFiles ; Redimension /N = (N7) selvslitFiles
				vslitFiles[N7 - 1] = nxsScansFiles[i]
				break							
					
			endswitch		

		HDF5CloseGroup	GroupID
		HDF5CloseFile FileID

	endfor
	
	
	
	// Construction des waves des listBox des panels : 
		//	BeamlineAlignement (energy + fent_h1 + vslit)
		//	XRayAbsorption (XAS)
		//	SampleAlignement (Sample-tx, Sample-tz)
		//	RIXS (RIXS)
	
	concatenate /O/NP=0 {MonoFiles, fent_h1Files, vslitFiles}, root:RIXS:Variables:BeamlineAlignement
	Make /O/B/N=(N3 + N6 + N7) root:RIXS:Variables:selBeamlineAlignement = 0
	
	// Plus n�cessaire car sample_tx et sample_tz sont dans des listBox diff�rentes -->  RIXS_sampleAlignement_DLG()
	// concatenate /O/NP=0 {Sample_txFiles, Sample_tzFiles}, root:RIXS:Variables:SampleAlignement
	// Make /O/B/N=(N4 + N5) root:RIXS:Variables:selSampleAlignement = 0
	// Remplac� par :Sample_txFiles
	Make /T/O/N=(N4) root:RIXS:Variables:sampleAlignement_tx = Sample_txFiles
	Make /O/B/N=(N4) root:RIXS:Variables:selSampleAlignement_tx = 0
	
	Make /T/O/N=(N5) root:RIXS:Variables:sampleAlignement_tz = Sample_tzFiles
	Make /O/B/N=(N5) root:RIXS:Variables:selSampleAlignement_tz = 0


	Make /T/O/N=(N2) root:RIXS:Variables:XRayAbsorption = XASfiles
	Make /O/B/N=(N2) root:RIXS:Variables:selXRayAbsorption = 0
	
	Make /T/O/N=(N1) root:RIXS:Variables:RIXS = RIXSfiles
	Make /O/B/N=(N1) root:RIXS:Variables:selRIXS = 0
	
	// RIXS_distrubutionFichiers1D()
								
	return 0
end




______________________________________________________________________

Chargement et distribution des fichiers 1D (du type scan_007.nxs) au sein
des Folders associ�s :

BeamlineAlignement (MonoFiles, fent_h1Files, vslitFiles)		==>	root:RIXS:panels:beamlineAlignementFolder
SampleAlignement (Sample_txFiles, Sample_tzFiles)				==>	root:RIXS:panels:sampleAlignementFolder
XRayAbsorption (XASfiles)												==>	root:RIXS:panels:XRayAbsorptionFolder
RIXS (RIXSfiles)															==>	root:RIXS:panels:RIXSFolder


______________________________________________________________________
function RIXS_distrubutionFichiers1D()

	SVAR dataPathFile = root:RIXS:Variables:dataPathFile
	
	WAVE /T BeamlineAlignement = root:RIXS:Variables:BeamlineAlignement
	WAVE /T SampleAlignement = root:RIXS:Variables:SampleAlignement
	WAVE /T XRayAbsorption = root:RIXS:Variables:XRayAbsorption
	WAVE /T RIXS = root:RIXS:Variables:RIXS
	

	// Dimensions des diff�rentes waves BeamlineAlignement, SampleAlignement, XRayAbsorption, RIXS
	Make /O/N=4 dim = {numPnts(BeamlineAlignement), numPnts(SampleAlignement), numPnts(XRayAbsorption), numPnts(RIXS)}
	
	// Folders de rangement des waves BeamlineAlignement, SampleAlignement, XRayAbsorption, RIXS
	string repDestination = "root:RIXS:panels:beamlineAlignementFolder;root:RIXS:panels:SampleAlignementFolder;root:RIXS:panels:XRayAbsorptionFolder;root:RIXS:panels:RIXSFolder"
	
	string previous_DF
	variable i, j, k
	variable FileID, GroupID	
	string cible					// cible pour re�oir le nom du fichier.nxs (ex : scan_007.nxs)
	string groupePrincipal, dataSets_list, listItem
	variable taille_list 
	variable goFrom, charComp
	
	
	for (i = 0 ; i <= 3 ; i += 1)					// s�lection folder de rangement
	
		previous_DF = getDataFolder(1)
		setDataFolder stringFromList(i, repDestination, ";")			// Placement dans le r�pertoire de destination
		
			
			for (j = 0 ; j <= dim[i] - 1 ; j +=1)							// Chargement des waves voulues dans le folder de rangement
			
				if (i == 0)
				
					cible = BeamlineAlignement[j] //; print "BeamlineAlignement[",j,"]", BeamlineAlignement[j]
				elseif(i == 1)
				
					cible = SampleAlignement[j] //; print "SampleAlignement[",j,"]", SampleAlignement[j]
				elseif(i == 2)
				
					cible = XRayAbsorption[j] //; print "XRayAbsorption[",j,"]", XRayAbsorption[j]
				elseif(i == 3)
				
					cible = RIXS[j] //; print "RIXS[",j,"]", RIXS[j]
				endif
				
					
				HDF5OpenFile /R FileID as dataPathFile + cible			// Chargement fichier.nxs
				
				HDF5Dump /A /ATTR = 0 /Q dataPathFile + cible				// R�cup�ration du nom du groupe principal 
				goFrom = strSearch(S_HDF5Dump, "\"scan", 0)
				k = 1
					do
					
						charComp = cmpstr(S_HDF5Dump[goFrom + k], "\"")
						k += 1
					while(charComp != 0)
					
				groupePrincipal = S_HDF5Dump[goFrom + 1, goFrom + k-2] //; print "RIXS_loadNXS() : groupePrincipal -->", groupePrincipal
				
				HDF5OpenGroup FileID, groupePrincipal + "/scan_data", GroupID
				
				HDF5ListGroup FileID, groupePrincipal + "/scan_data"
				dataSets_list = S_HDF5ListGroup //; print "dataSets_list -->", dataSets_list //; print " "
				
				
				// Chargement des data de la dataSets_list. Pr�fixer chaque fichier d'un "groupePrincipal_"
				taille_list = itemsInList(dataSets_list, ";")
				
					for(k = 0 ; k <= taille_list - 1 ; k += 1)
						listItem = stringFromList(k, dataSets_list, ";")
						HDF5LoadData/O/Q/VAR=0/N=$listItem GroupID, listItem
						duplicate /O $listItem, $(groupePrincipal + "_" + listItem) ; killwaves $listItem
					endfor				
				
				
				HDF5CloseGroup	GroupID
				HDF5CloseFile FileID  
			
			
			endfor 
			
		setDataFolder previous_DF
	endfor


	return 0
end





______________________________________________________________________

Purge des r�pertoires lors du passage entre deux chargements de fichiers.nxs 

______________________________________________________________________
function RIXS_purge()

	string previous_DF = getDataFolder(1)
	
	setDataFolder root:RIXS:panels:beamlineAlignementFolder	; killWaves /A /Z
	setDataFolder root:RIXS:panels:XRayAbsorptionFolder			; killWaves /A /Z
	setDataFolder root:RIXS:panels:sampleAlignementFolder		; killWaves /A /Z
	setDataFolder root:RIXS:panels:RIXSFolder						; killWaves /A /Z
	
	setDataFolder root:RIXS:infos_Problemes							; killWaves /A /Z
	setDataFolder root:RIXS:buffer										; killWaves /A /Z
	setDataFolder root:RIXS:buffer:contextualData					; killWaves /A /Z
	
	setDataFolder previous_DF

	return 0
end






______________________________________________________________________

Au clic sur un fichier.nxs dans une listBox :

1 / r�cup�re le groupe principal ie le m�me nom que celui du fichier.nxs (au bug incr�mental pr�s)
2 / A partir de l� : utilisation des fonctions HDF5 pour r�cup�rer les listes dataSetList et dataSetAttrList (ici ne sert � rien, juste pour info)
3 / On Dump sur le groupe scan_data pour r�cup�rer l'adresse tango (type du scan en fait) --> pour les data contextuelles
4 / On Dump sur le groupe SEXTANTS pour r�cup�rer : gap, phase, 

______________________________________________________________________

function RIXS_loadNXS()

	SVAR dataPathFile  = root:RIXS:Variables:dataPathFile
	SVAR nxsSelected = root:RIXS:Variables:nxsSelected
	SVAR nxsGroup = root:RIXS:Variables:nxsGroup
	SVAR racineList = root:RIXS:Variables:racineList
	SVAR dataSetsList = root:RIXS:Variables:dataSetsList
	SVAR dataSetAttrList = root:RIXS:Variables:dataSetAttrList
	SVAR adresseTango = root:RIXS:Variables:adresseTango
	NVAR dimImage = root:RIXS:Variables:dimImage
	
	WAVE /T scanMetaDataWave = root:RIXS:Variables:scanMetaDataWave
	WAVE selScanMetaDataWave = root:RIXS:Variables:selScanMetaDataWave
	
	variable FileID, GroupID
	
	
	//string nxsGroup = RIXS_scanGroupNameCorrected()
	
	// A la p�che des data et metadata du scan au sein du scan_xyz.nxs
	HDF5OpenFile /R FileID as dataPathFile + nxsSelected
	
		// Dump pour r�cup�rer le nom du "Group" du scan (ici nxsGroup). En fait le nom du fichier.nxs sans l'extension .nxs 
		// et � l'indexation pr�s (bug ? Il n'y a pas forc�ment co�ncidence entre l'index du fichier et celui du "Group". Diff�rence d'1).
		HDF5Dump /A /ATTR = 0 /Q dataPathFile + nxsSelected 
		variable goFrom = strSearch(S_HDF5Dump, "\"scan", 0)
		variable i = 1, charComp
		do
		
			charComp = cmpstr(S_HDF5Dump[goFrom + i], "\"")
			i += 1
		while(charComp != 0)
		
		nxsGroup = S_HDF5Dump[goFrom + 1, goFrom + i-2] //; print "RIXS_loadNXS() : nxsGroup -->", nxsGroup

			
			// Groupe scan_data -----------------------------------------------------------
			HDF5OpenGroup FileID, nxsGroup + "/scan_data", GroupID
						
				// Liste Datasets 
				HDF5ListGroup FileID, nxsGroup + "/scan_data" 
				dataSetsList = S_HDF5ListGroup //; print "RIXS_loadNXS() : dataSetsList -->", dataSetsList
				
					// Liste Dataset Attributes (sur le 1er elt de dataSetsList)
					HDF5ListAttributes GroupID, stringFromList(0, dataSetsList, ";") 
					dataSetAttrList = S_HDF5ListAttributes //; print "RIXS_loadNXS() : dataSetAttrList -->", dataSetAttrList
								
						// R�cup�ration de l'adresse Tango (celle correspondant au 1er �l�ment de la dataSetsList)
						HDF5Dump /A = nxsGroup + "/scan_data/" + stringFromList(0, dataSetsList, ";") + "/long_name" /Q dataPathFile + nxsSelected 
						
							goFrom = strsearch(S_HDF5Dump, "i14-m", 0)
							i = 0
							do
								charComp = cmpstr(S_HDF5Dump[goFrom + i], "\"")
								i += 1 
							while(charComp != 0)
							adresseTango = S_HDF5Dump[goFrom, goFrom + i - 2] //; print "adresseTango ==>", adresseTango
							
								// Si adresse r�cup�r�e correnspond � une image ie i14-m-cx1/dt/princeton.rixs.1/image on r�cup�re sa dimension
								if(cmpstr(adresseTango, "i14-m-cx1/dt/princeton.rixs.1/image") == 0)
		
									HDF5Dump /Q /ATTR=0 /H=(!0) /D = nxsGroup + "/scan_data/data_01" dataPathFile + nxsSelected 
																			
									goFrom = strsearch(S_HDF5Dump, ",", 0)
									i = 1
									do
										charComp = cmpstr(S_HDF5Dump[goFrom + i], ",")
										i += 1
									while(charComp != 0)
									
										dimImage = str2num(S_HDF5Dump[goFrom + 1, goFrom + i - 1])
								endif
		
		
														
							
// Chargement des data du groupe Datasets dans root:RIXS:buffer --------------------------------------------
							variable tailleListe = itemsInList(dataSetsList, ";")
							string listItem
							
							string previous_DF = getDataFolder(1)
							setDataFolder root:RIXS:buffer
							
								killWaves /A /Z ; killVariables /A /Z ; killStrings /A /Z
								for(i = 0 ; i <= tailleListe - 1 ; i += 1)
									listItem = stringFromList(i, dataSetsList, ";")
									HDF5LoadData/O/Q/VAR=0/N=$listItem GroupID, listItem
								endfor
							setDataFolder previous_DF
		
		HDF5CloseGroup	GroupID		// ferme groupe scan_data
	
	HDF5CloseFile FileID
	




// Chargement des donn�es contextuelles dans root:RIXS:buffer:contextualData -------------------------
	RIXS_infosContextuelles(nxsSelected)

	
	
			
			
// PLOT 1D ou 2D (image RIXS) ------------------------------------------------------------------------
			// centralPanel --> effacement si data_01 ou actuator_1_1 en pr�sence
			
				removeImage /Z /W = RIXScentralPanel#visuel data_01, data_02
				removeFromGraph /Z /W = RIXScentralPanel#visuel data_01			// actuator_1_1	
				removeImage /Z /W = RIXSspectraPanel#visu data_01, data_02
				removeFromGraph /Z /W = RIXSspectraPanel#visu data_01			// actuator_1_1			
		
			
			if (cmpstr(adresseTango, "i14-m-cx1/dt/princeton.rixs.1/image") == 0)		// type 2D : Image RIXS
			
				// Passage image 1D � image 2D
				WAVE data_01 = root:RIXS:buffer:data_01
				Make /O /N = (dimImage, dimImage) root:RIXS:buffer:data_02
				WAVE data_02 = root:RIXS:buffer:data_02
				//newImage data_02
				data_02[][] = data_01[0][p][q]
				
				// centralPanel (#visuel)
				AppendImage /W = RIXScentralPanel#visuel data_02
				ModifyImage /W = RIXScentralPanel#visuel data_02 ctab= {*,*,Copper,0}
				ColorScale /W = RIXScentralPanel#visuel /C/N=text0/F=0/A=RC/E image=data_02,frame=0.00
				Label /Z /W=RIXSspectraPanel#visu bottom nxsSelected + "  ---  " + adresseTango
			
				// spectraPanel (#visu)
				AppendImage /W = RIXSspectraPanel#visu data_02
				ModifyImage /W = RIXSspectraPanel#visu data_02 ctab= {*,*,Copper,0}
				ColorScale/W = RIXSspectraPanel#visu /C/N=text0/F=0/A=RC/E image=data_02,frame=0.00
				Label /Z /W=RIXSspectraPanel#visu bottom nxsSelected + "  ---  " + adresseTango				

			else		// type actuator 1D : energy, position...
				
				WAVE data_01 = root:RIXS:buffer:data_01		// actuator_1_1	
				
				AppendToGraph /W = RIXScentralPanel#visuel data_01		// actuator_1_1	
				Label /Z /W=RIXScentralPanel#visuel left "\\Z14Diode signal (a.u.)"
				Label /Z /W=RIXScentralPanel#visuel bottom nxsSelected + "  ---  " + adresseTango
				modifyGraph /W = RIXScentralPanel#visuel frameStyle = 0
				
				AppendToGraph /W = RIXSspectraPanel#visu data_01			// actuator_1_1	
				Label /Z /W=RIXSspectraPanel#visu left "\\Z14Diode signal (a.u.)"
				Label /Z /W=RIXSspectraPanel#visu bottom nxsSelected + "  ---  " + adresseTango
				modifyGraph	/W = RIXSspectraPanel#visu frameStyle = 0		
			
			endif
			
	return 0
end

titlebox




______________________________________________________________________
Pour �dition table "editScanType" des fichiers r�ellement charg�s apr�s filtrage des mauvais.
"editScanType" �tablit la correspondance entre le nom d'un fichier et l'adresse tango correspondant
au type du fichier (RIXS, energy (mono), beamlineEnergy, sample-tx,...)

Sortie : wave nxsScansFile_fileType => Edit. Accessible aussi via le menu (Edit --> file.nxs - type)

nxsScansFiles d�j� conditionn� par : RIXS_chargementNXS()
______________________________________________________________________

function RIXS_nxsFilesTypes()

	SVAR dataPathFile  = root:RIXS:Variables:dataPathFile
	WAVE /T nxsScansFiles = root:RIXS:Variables:nxsScansFiles
	WAVE /T nxsScansFile_fileType = root:RIXS:Variables:nxsScansFile_fileType
	SVAR adresseTango = root:RIXS:Variables:adresseTango
	
	string data_SetsList, data_SetAttrList, nxs_Group
	
	variable FileID, GroupID, i, nbFichiersNxs = numpnts(nxsScansFiles)
	redimension /N=(nbFichiersNxs, 2) nxsScansFile_fileType
	
	variable j = 1, charComp
	
	for (i = 0 ; i <= nbFichiersNxs - 1 ; i += 1)
		
		HDF5OpenFile /R FileID as dataPathFile + nxsScansFiles[i]
		
			// Dump pour r�cup�rer le nom du "Group" du scan 
			HDF5Dump /A /ATTR = 0 /Q dataPathFile + nxsScansFiles[i]
			variable goFrom = strSearch(S_HDF5Dump, "\"scan", 0)
			j = 1
			do
			
				charComp = cmpstr(S_HDF5Dump[goFrom + j], "\"")
				j += 1
			while(charComp != 0)
			
			nxs_Group = S_HDF5Dump[goFrom + 1, goFrom + j-2]
		
	
				// Groupe scan_data -----------------------------------------------------------
				HDF5OpenGroup FileID, nxs_Group + "/scan_data", GroupID
							
					// Liste Datasets 
					HDF5ListGroup FileID, nxs_Group + "/scan_data" 
					data_SetsList = S_HDF5ListGroup //; print "dataSetsList -->", dataSetsList
					
//						// Liste Dataset Attributes (sur le 1er elt de dataSetsList)
//						HDF5ListAttributes GroupID, stringFromList(0, data_SetsList, ";") 
//						data_SetAttrList = S_HDF5ListAttributes //; print "dataSetAttrList -->", dataSetAttrList
									
							// R�cup�ration de l'adresse Tango (celle correspondant au 1er �l�ment de la dataSetsList)
							HDF5Dump /A = nxs_Group + "/scan_data/" + stringFromList(0, data_SetsList, ";") + "/long_name" /Q dataPathFile + nxsScansFiles[i] 
							
								goFrom = strsearch(S_HDF5Dump, "i14-m", 0)
								j = 0
								do
									charComp = cmpstr(S_HDF5Dump[goFrom + j], "\"")
									j += 1 
								while(charComp != 0)
								adresseTango = S_HDF5Dump[goFrom, goFrom + j - 2] 
															
			HDF5CloseGroup	GroupID		// ferme groupe scan_data
			
		HDF5CloseFile FileID
		
		// Remplissage wave Fichier.nxs <=> adresseTango (ie type)
		nxsScansFile_fileType[i][0] = nxsScansFiles[i]
		nxsScansFile_fileType[i][1] = adresseTango	
	endfor
	
	Edit /N=editScanType /K=1 /W=(592,181,1274,797) root:RIXS:Variables:nxsScansFile_fileType
	ModifyTable /W=editScanType format(Point)=1,width(nxsScansFile_fileType)=238
	
	return 0
end





______________________________________________________________________

	INFOS DONNEES CONTEXTUELLES
	
	nxsFile = fichier.nxs
		
		GROUPE PARENT	=	nxsGroup (le nom du fichier.nxs (nxsFile) au bug d'indexation pr�s)
		
			Donn�es contextuelles :
				Onduleurs HU44 et HU80 :
					sous groupe	>> groupName = SEXTANTS/ANS-C14__M-HU44.1_ENERGY__#1	>> Datasets Dump(gap, phase, polarisation)
					sous groupe	>> groupName = SEXTANTS/ANS-C14__M-HU80.2_ENERGY__#1	>> Datasets Dump(gap, phase, polarisation)
				
				Monochromateur
					sous groupe	>> groupName = SEXTANTS/Monochromator

			Donn�es exp�rimentales :
				sous groupe	>> groupName = scan_data	>>	Datasets Dump(actuator_1_1, data_01, ....)	& Dataset Attribute Dump(long_name)
				
				
				
- Changer le nom --> "File Metadata" ou similaire
- Affichier dans le champ a droite de la liste de fichiers les donnais contextuels suivants (Je donne ici le chemin:
  
  GROUP/DataSet/value):
  	  Monochromator/energy/value
	  Monochromator/resolution/value
	  Monochromator/current_slot_name/value
	  Monochromator/exit_slit_aperture/value
	  Monochromator/grating_position/value
	  
	  "ANS-C14__EI__M-HU44.1_ENERGY__#1"/gap/value
	  "ANS-C14__EI__M-HU44.1_ENERGY__#1"/phase/value
	  "ANS-C14__EI__M-HU44.1_ENERGY__#1"/phase/value
	  "ANS-C14__EI__M-HU80.2_ENERGY__#1"/gap/value
	  "ANS-C14__EI__M-HU80.2_ENERGY__#1"/phase/value
	  "ANS-C14__EI__M-HU80.2_ENERGY__#1"/phase/value
   
 	
______________________________________________________________________

function RIXS_infosContextuelles(nxsFile)		// RIXS_infosContextuelles("scan_003.nxs")
	string nxsFile

	SVAR dataPathFile  = root:RIXS:Variables:dataPathFile
	WAVE /T scanMetaDataWave = root:RIXS:Variables:scanMetaDataWave
	WAVE selScanMetaDataWave = root:RIXS:Variables:selScanMetaDataWave
	SVAR adresseTango = root:RIXS:Variables:adresseTango
	
	string nxs_Group, groupNameList, dataSets_List, dataSetAttr_List, listItem
	variable FileID, GroupID, taille_dataSets_List, currentDim = 1
	


	// M�nage pour recevoir le nouveau fichier.nxs
		
		// Initialisation de scanMetaDataWave et selScanMetaDataWave � 0
		redimension /N=1 scanMetaDataWave ; redimension /N=1 selScanMetaDataWave
		scanMetaDataWave[0] = "Acquisition type --> " + adresseTango
		
		// On efface les donn�es contextuelles du fichier.nxs pr�c�dent
		string previous_DF = getDataFolder(1)
		setDataFolder root:RIXS:buffer:contextualData
	
			killWaves /A /Z ; killVariables /A /Z ; killStrings /A /Z
		setDataFolder previous_DF



	// A la p�che des donn�es contextuelles du fichier.nxs
	
	HDF5OpenFile /R FileID as dataPathFile + nxsFile
	
		// Dump pour r�cup�rer le nom du GROUPE PARENT du scan (ie le nom du fichier.nxs --> nxGroup = fichier au bug de l'indice pr�s...) 
		HDF5Dump /A /ATTR = 0 /Q dataPathFile + nxsFile 
		variable goFrom = strSearch(S_HDF5Dump, "\"scan", 0)		// Sur SEXTANTS le GROUPE PARANT commence par "scan" (+ "qq chose")
		variable i = 1, charComp
		do
		
			charComp = cmpstr(S_HDF5Dump[goFrom + i], "\"")
			i += 1
		while(charComp != 0)
		nxs_Group = S_HDF5Dump[goFrom + 1, goFrom + i-2]		// GROUPE PARENT 
		
		
		// Liste des 3 groupes o� il faut r�cup�rer les donn�es contextuelles (1mono et 2 ondul)
		groupNameList = "SEXTANTS/Monochromator;SEXTANTS/ANS-C14__EI__M-HU44.1_ENERGY__#1;SEXTANTS/ANS-C14__EI__M-HU80.2_ENERGY__#1"
		string equipement = "Monochromateur;HU44;HU80"
		variable j
		
				
		for (i = 0 ; i <= 2 ; i += 1)
		
			// Ouverture du groupe cible (groupNameList) -----------------------------------------------------------
			HDF5OpenGroup FileID, nxs_Group + "/" + stringFromList(i, groupNameList, ";"), GroupID
							
				// Liste Datasets 
				HDF5ListGroup FileID, nxs_Group + "/" + stringFromList(i, groupNameList, ";")
				dataSets_List = S_HDF5ListGroup //; print "dataSets_List -->", dataSets_List
				taille_dataSets_List = itemsInList(dataSets_List, ";")
				
				// Chargement des donn�es de dataSets_List (DataSets) 
								
							previous_DF = getDataFolder(1)
							setDataFolder root:RIXS:buffer:contextualData
														
								for(j = 0 ; j <= taille_dataSets_List - 1 ; j += 1)
									listItem = stringFromList(j, dataSets_List, ";")
									HDF5LoadData/O/Q/VAR=0/N=$listItem GroupID, listItem
									if(V_Flag == 0)
										
										currentDim += 1
										redimension /N=(currentDim) scanMetaDataWave
										redimension /N=(currentDim) selScanMetaDataWave
										
										// Trie entre les WAVES textes et num�riques
										if(cmpstr(listItem, "current_grating_name") == 0 || cmpstr(listItem, "current_slot_name") == 0)
										
											//print "SVAR : listItem -->", listItem
											WAVE /T str = $listItem
											scanMetaDataWave[currentDim - 1] = stringFromList(i, equipement, ";") + " ; " + listItem + " ---> " + str[0]
										else
										
											//print "NVAR : listItem -->", listItem
											WAVE val = $listItem
											scanMetaDataWave[currentDim - 1] = stringFromList(i, equipement, ";") + " ; "  + listItem + " ---> " + num2str(val[0])
										endif
																			
									else
									
										doAlert /T="Probl�me niveau RIXS_infosContextuelles(nxsFile)", 0, "Probl�me de chargement HDF5 sur :" + stringFromList(j, dataSets_List, ";")
									endif
								endfor
								
								
							setDataFolder previous_DF
				//__________________________________________________


				
			HDF5CloseGroup	GroupID		// ferme groupe scan_data
		endfor						
						
	HDF5CloseFile FileID
	
	
	
	
	// MAJ Meta Data en fonction de nxsSelected
//	redimension /N=(nbElts + 1) scanMetaDataWave
//	redimension /N=(nbElts + 1) selScanMetaDataWave
//	scanMetaDataWave[0] = adresseTango
//		for (i = 1 ; i <= nbElts ; i += 1)
//					
//			scanMetaDataWave[i] = stringFromList(i - 1, dataSetsList, ";")
//
//		endfor
	
	
	return 0
end










____________________________________________________________________________________________________________________________________






